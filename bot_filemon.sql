-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2020 at 05:57 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bot_filemon`
--

-- --------------------------------------------------------

--
-- Table structure for table `group_telegram`
--

CREATE TABLE `group_telegram` (
  `id` int(10) NOT NULL,
  `id_group_telegram` varchar(20) NOT NULL,
  `arr_id_user` text NOT NULL,
  `label_group` varchar(20) NOT NULL,
  `id_create_user` int(10) NOT NULL,
  `tgl_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `monitoring`
--

CREATE TABLE `monitoring` (
  `id` int(100) NOT NULL,
  `id_token` int(10) NOT NULL,
  `mode_file` enum('M','D','C') NOT NULL,
  `filename` text NOT NULL,
  `scan_detector` text NOT NULL,
  `encryption` text NOT NULL,
  `ignore_file` text DEFAULT NULL,
  `tgl_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `target_monitoring`
--

CREATE TABLE `target_monitoring` (
  `id` int(10) NOT NULL,
  `id_telegram` varchar(20) NOT NULL,
  `directory` text NOT NULL,
  `description` text NOT NULL,
  `label_target` text NOT NULL,
  `token` text NOT NULL,
  `id_create_user` int(10) NOT NULL,
  `ip_dns` varchar(25) DEFAULT NULL,
  `link_apps` varchar(50) NOT NULL,
  `status` enum('personal','group') NOT NULL,
  `file_init` text DEFAULT NULL,
  `tgl_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

CREATE TABLE `user_log` (
  `id` int(10) NOT NULL,
  `nik` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `jabatan` varchar(30) NOT NULL,
  `id_telegram` varchar(20) DEFAULT NULL,
  `password` text NOT NULL,
  `level` enum('admin','operator','view') NOT NULL,
  `tgl_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_log`
--

INSERT INTO `user_log` (`id`, `nik`, `nama`, `alamat`, `no_telp`, `email`, `jabatan`, `id_telegram`, `password`, `level`, `tgl_update`) VALUES
(6, '101010101010', 'SUPER ADMIN', 'JL. INDONESIA RAYA NO. 46', '089677271257', 'admin@super.com', 'ADMIN', '430265248', '0eff44c362b13fa25fc88a412f5512e1', 'admin', '2020-03-28 00:41:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `group_telegram`
--
ALTER TABLE `group_telegram`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monitoring`
--
ALTER TABLE `monitoring`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `target_monitoring`
--
ALTER TABLE `target_monitoring`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_log`
--
ALTER TABLE `user_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `group_telegram`
--
ALTER TABLE `group_telegram`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `monitoring`
--
ALTER TABLE `monitoring`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `target_monitoring`
--
ALTER TABLE `target_monitoring`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_log`
--
ALTER TABLE `user_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
