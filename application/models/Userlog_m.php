<?php


class Userlog_m extends CI_Model{
	public function __construct()
	{
		parent::__construct();
	}

	public function getAll(){
		return $this->db->get('user_log')->result_array();
	}

	public function getWhere($id){
		return $this->db->get_where('user_log', array('id' => $id))->row();
	}

	public function getWherePass($id){
		return $this->db->get_where('user_log', array('password' => $id));
	}

	public function getCount(){
		return $this->db->count_all('user_log');
	}

	public function save($data){
		return $this->db->insert('user_log', $data);
	}

	public function edit($id, $data){
		return $this->db->update('user_log', $data, array('id' => $id));
	}

	public function hapus($id){
		return $this->db->delete('user_log', array('id' => $id));
	}

	public function auth($email, $pass){
		return $this->db
			->select("*")
			->from("user_log")
			->where("email", $email)
			->where("password", $pass)
			->get();
	}
}
