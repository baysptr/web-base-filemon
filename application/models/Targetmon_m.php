<?php


class Targetmon_m extends CI_Model{
	public function __construct()
	{
		parent::__construct();
	}

	public function getAll(){
		return $this->db->get('target_monitoring')->result_array();
	}

	public function getAllno(){
		$this->db->select('*');
		$this->db->where('file_init is NOT NULL', NULL, FALSE);
		return $this->db->get('target_monitoring')->result_array();
	}

	public function getWhere($id){
		return $this->db->get_where('target_monitoring', array('id' => $id))->row();
	}

	public function getCheck($id){
		return $this->db->get_where('target_monitoring', array('id' => $id))->num_rows();
	}

	public function getWhereToken($id){
		return $this->db->get_where('target_monitoring', array('token' => $id));
	}

	public function getCount(){
		return $this->db->count_all('target_monitoring');
	}

	public function save($data){
		return $this->db->insert('target_monitoring', $data);
	}

	public function edit($id, $data){
		return $this->db->update('target_monitoring', $data, array('id' => $id));
	}

	public function hapus($id){
		return $this->db->delete('target_monitoring', array('id' => $id));
	}
}
