<?php


class Layout_m extends CI_Model{
	public function __construct()
	{
		parent::__construct();
	}
	public function head($title){
		$result = $title . " | File Monitoring";
		$html = '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
				<meta name="description" content="Sistem untuk controling aplikasi yang sedang berjalan pada suatu server dengan dukungan notifikasi pada telegram">
				<meta name="keywords" content="monitoring, aplikasi, telegram">
				<meta name="author" content="@baysptr">
				<title>'.$result.'</title>
				<link rel="apple-touch-icon" href="'.base_url().'assets/images/ico/apple-icon-120.png">
				<link rel="shortcut icon" type="image/x-icon" href="'.base_url().'assets/images/ico/favicon.ico">
				<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">
				<link rel="stylesheet" type="text/css" href="'.base_url().'assets/vendors/css/vendors.min.css">
				<link rel="stylesheet" type="text/css" href="'.base_url().'assets/vendors/css/charts/apexcharts.css">
				<link rel="stylesheet" type="text/css" href="'.base_url().'assets/vendors/css/extensions/dragula.min.css">
				<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/bootstrap.min.css">
				<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/bootstrap-extended.min.css">
				<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/colors.min.css">
				<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/components.min.css">
				<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/themes/dark-layout.min.css">
				<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/themes/semi-dark-layout.min.css">
				<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/core/menu/menu-types/horizontal-menu.min.css">
				<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/pages/dashboard-analytics.min.css">
				<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/style.css">';
		return $html;
	}
	public function header($menu_isactive){
		$active = '';
		if($menu_isactive=="manage_user"){
			//manage_user
			$active = '<li class="nav-item"><a class="nav-link" href="'.site_url().'dashboard"><i class="menu-livicon" data-icon="desktop"></i><span data-i18n="Dashboard">Dashboard</a></li>
						<li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="menu-livicon" data-icon="notebook"></i><span>User and Group</span></a>
							<ul class="dropdown-menu">
								<li class="active" data-menu=""><a class="dropdown-item align-items-center" href="#" data-toggle="dropdown"><i class="bx bx-right-arrow-alt"></i>Manage User</a></li>
								<li data-menu=""><a class="dropdown-item align-items-center" href="'.site_url().'manage_group" data-toggle="dropdown"><i class="bx bx-right-arrow-alt"></i>Manage Group</a></li>
							</ul>
						</li>
						<li class="nav-item"><a class="nav-link" href="'.site_url().'manage_target"><i class="menu-livicon" data-icon="thumbnails-big"></i>Control and Monitoring</a></li>
						<li class="nav-item"><a class="nav-link" href="'.site_url().'report"><i class="menu-livicon" data-icon="morph-folder"></i>Report</a></li>';
		}elseif ($menu_isactive=="manage_group"){
			//manage_group
			$active = '<li class="nav-item"><a class="nav-link" href="'.site_url().'dashboard"><i class="menu-livicon" data-icon="desktop"></i><span data-i18n="Dashboard">Dashboard</a></li>
						<li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="menu-livicon" data-icon="notebook"></i><span>User and Group</span></a>
							<ul class="dropdown-menu">
								<li data-menu=""><a class="dropdown-item align-items-center" href="'.site_url().'manage_user" data-toggle="dropdown"><i class="bx bx-right-arrow-alt"></i>Manage User</a></li>
								<li class="active" data-menu=""><a class="dropdown-item align-items-center" href="'.site_url().'manage_group" data-toggle="dropdown"><i class="bx bx-right-arrow-alt"></i>Manage Group</a></li>
							</ul>
						</li>
						<li class="nav-item"><a class="nav-link" href="'.site_url().'manage_target"><i class="menu-livicon" data-icon="thumbnails-big"></i>Control and Monitoring</a></li>
						<li class="nav-item"><a class="nav-link" href="'.site_url().'report"><i class="menu-livicon" data-icon="morph-folder"></i>Report</a></li>';
		}elseif ($menu_isactive=="dashboard"){
			//dashboard
			$active = '<li class="active nav-item"><a class="nav-link" href="'.site_url().'dashboard"><i class="menu-livicon" data-icon="desktop"></i><span data-i18n="Dashboard">Dashboard</a></li>
						<li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="menu-livicon" data-icon="notebook"></i><span>User and Group</span></a>
							<ul class="dropdown-menu">
								<li data-menu=""><a class="dropdown-item align-items-center" href="'.site_url().'manage_user" data-toggle="dropdown"><i class="bx bx-right-arrow-alt"></i>Manage User</a></li>
								<li data-menu=""><a class="dropdown-item align-items-center" href="'.site_url().'manage_group" data-toggle="dropdown"><i class="bx bx-right-arrow-alt"></i>Manage Group</a></li>
							</ul>
						</li>
						<li class="nav-item"><a class="nav-link" href="'.site_url().'manage_target"><i class="menu-livicon" data-icon="thumbnails-big"></i>Control and Monitoring</a></li>
						<li class="nav-item"><a class="nav-link" href="'.site_url().'report"><i class="menu-livicon" data-icon="morph-folder"></i>Report</a></li>';
		}elseif ($menu_isactive=="manage_target"){
			//manage_target
			$active = '<li class="nav-item"><a class="nav-link" href="'.site_url().'dashboard"><i class="menu-livicon" data-icon="desktop"></i><span data-i18n="Dashboard">Dashboard</a></li>
						<li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="menu-livicon" data-icon="notebook"></i><span>User and Group</span></a>
							<ul class="dropdown-menu">
								<li data-menu=""><a class="dropdown-item align-items-center" href="'.site_url().'manage_user" data-toggle="dropdown"><i class="bx bx-right-arrow-alt"></i>Manage User</a></li>
								<li data-menu=""><a class="dropdown-item align-items-center" href="'.site_url().'manage_group" data-toggle="dropdown"><i class="bx bx-right-arrow-alt"></i>Manage Group</a></li>
							</ul>
						</li>
						<li class="active nav-item"><a class="nav-link" href="'.site_url().'manage_target"><i class="menu-livicon" data-icon="thumbnails-big"></i>Control and Monitoring</a></li>
						<li class="nav-item"><a class="nav-link" href="'.site_url().'report"><i class="menu-livicon" data-icon="morph-folder"></i>Report</a></li>';
		}elseif ($menu_isactive=="report"){
			//report
			$active = '<li class="nav-item"><a class="nav-link" href="'.site_url().'dashboard"><i class="menu-livicon" data-icon="desktop"></i><span data-i18n="Dashboard">Dashboard</a></li>
						<li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="menu-livicon" data-icon="notebook"></i><span>User and Group</span></a>
							<ul class="dropdown-menu">
								<li data-menu=""><a class="dropdown-item align-items-center" href="'.site_url().'manage_user" data-toggle="dropdown"><i class="bx bx-right-arrow-alt"></i>Manage User</a></li>
								<li data-menu=""><a class="dropdown-item align-items-center" href="'.site_url().'manage_group" data-toggle="dropdown"><i class="bx bx-right-arrow-alt"></i>Manage Group</a></li>
							</ul>
						</li>
						<li class="nav-item"><a class="nav-link" href="'.site_url().'manage_target"><i class="menu-livicon" data-icon="thumbnails-big"></i>Control and Monitoring</a></li>
						<li class="active nav-item"><a class="nav-link" href="'.site_url().'report"><i class="menu-livicon" data-icon="morph-folder"></i>Report</a></li>';
		}else{
			//profile
			$active = '<li class="nav-item"><a class="nav-link" href="'.site_url().'dashboard"><i class="menu-livicon" data-icon="desktop"></i><span data-i18n="Dashboard">Dashboard</a></li>
						<li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="menu-livicon" data-icon="notebook"></i><span>User and Group</span></a>
							<ul class="dropdown-menu">
								<li data-menu=""><a class="dropdown-item align-items-center" href="'.site_url().'manage_user" data-toggle="dropdown"><i class="bx bx-right-arrow-alt"></i>Manage User</a></li>
								<li data-menu=""><a class="dropdown-item align-items-center" href="'.site_url().'manage_group" data-toggle="dropdown"><i class="bx bx-right-arrow-alt"></i>Manage Group</a></li>
							</ul>
						</li>
						<li class="nav-item"><a class="nav-link" href="'.site_url().'manage_target"><i class="menu-livicon" data-icon="thumbnails-big"></i>Control and Monitoring</a></li>
						<li class="nav-item"><a class="nav-link" href="'.site_url().'report"><i class="menu-livicon" data-icon="morph-folder"></i>Report</a></li>';
		}
		$html = '<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-fixed bg-primary navbar-brand-center">
					<div class="navbar-header d-xl-block d-none">
						<ul class="nav navbar-nav flex-row">
							<li class="nav-item">
								<a class="navbar-brand" href="javascript:;">
									<div class="brand-logo"><img class="logo" src="'.base_url().'assets/images/logo/logo-light.png"></div>
									<h2 class="brand-text mb-0">File Monitoring</h2>
								</a>
							</li>
						</ul>
					</div>
					<div class="navbar-wrapper">
						<div class="navbar-container content">
							<div class="navbar-collapse" id="navbar-mobile">
								<div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
									<ul class="nav navbar-nav">
										<li class="nav-item mobile-menu mr-auto"><a class="nav-link nav-menu-main menu-toggle" href="#"><i class="bx bx-menu"></i></a></li>
									</ul>
									<ul class="nav navbar-nav bookmark-icons">
										<li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-todo.html" data-toggle="tooltip" data-placement="top" title="Manual User"><i class="ficon bx bx-info-circle"></i></a></li>
									</ul>
								</div>
								<ul class="nav navbar-nav float-right d-flex align-items-center">
									<li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon bx bx-fullscreen"></i></a></li>
									<li class="dropdown dropdown-user nav-item">
										<a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
											<div class="user-nav d-lg-flex d-none"><span class="user-name">'.$this->session->userdata('nama').'</span><span class="user-status">'.$this->session->userdata('jabatan').' - '.strtoupper($this->session->userdata('level')).'</span></div><span><img class="round" src="'.base_url().'assets/images/user.png" alt="avatar" height="40" width="40"></span></a>
										<div class="dropdown-menu dropdown-menu-right pb-0"><a class="dropdown-item" href="'.site_url().'profile">Edit Profile</a>
											<div class="dropdown-divider mb-0"></div><a class="dropdown-item" href="javascript:;" onclick="logout_standalone()"><i class="bx bx-power-off mr-50"></i> Logout</a>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</nav>
			
				<div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow" role="navigation" data-menu="menu-wrapper">
					<div class="navbar-header d-xl-none d-block">
						<ul class="nav navbar-nav flex-row">
							<li class="nav-item mr-auto">
								<a class="navbar-brand" href="index.html">
									<div class="brand-logo"><img class="logo" src="'.base_url().'assets/images/logo/logo.png" /></div>
									<h2 class="brand-text mb-0">Frest</h2>
								</a>
							</li>
							<li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary toggle-icon"></i></a></li>
						</ul>
					</div>
					<div class="shadow-bottom"></div>
					<div class="navbar-container main-menu-content" data-menu="menu-container">
						<ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="filled">
							'.$active.'
						</ul>
					</div>
				</div>';
		return $html;
	}
	public function footer(){
		$html = '<div class="sidenav-overlay"></div>
					<div class="drag-target"></div>
					<footer class="footer footer-static footer-light">
						<p class="clearfix mb-0"><span class="float-left d-inline-block">2020 &copy; FILE MONITORING</span><span class="float-right d-sm-inline-block d-none">Crafted with<i class="bx bxs-heart pink mx-50 font-small-3"></i>by<a class="text-uppercase" href="javascript:;" target="_blank">@baysptr</a></span>
							<button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
						</p>
					</footer>';
		return $html;
	}
	public function javascript(){
		//<script src="'.base_url().'assets/vendors/js/charts/apexcharts.min.js"></script>
		//<script src="'.base_url().'assets/js/scripts/pages/dashboard-analytics.min.js"></script>
		$html = '
			<script src="'.base_url().'assets/vendors/js/vendors.min.js"></script>
			<script src="'.base_url().'assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
			<script src="'.base_url().'assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.min.js"></script>
			<script src="'.base_url().'assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
			<script src="'.base_url().'assets/vendors/js/ui/jquery.sticky.js"></script>
			<script src="'.base_url().'assets/vendors/js/extensions/dragula.min.js"></script>
			<script src="'.base_url().'assets/js/scripts/configs/horizontal-menu.min.js"></script>
			<script src="'.base_url().'assets/js/core/app-menu.min.js"></script>
			<script src="'.base_url().'assets/js/core/app.min.js"></script>
			<script src="'.base_url().'assets/js/scripts/components.min.js"></script>
			<script src="'.base_url().'assets/js/scripts/footer.min.js"></script>
			<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
			<script>
			function logout_standalone() {
				Swal.fire({
					title: \'Apakah anda yakin?\',
					text: "Pastikan benar bahwa anda ingin keluar dari sistem ini!",
					icon: \'warning\',
					showCancelButton: true,
					confirmButtonColor: \'#3085d6\',
					cancelButtonColor: \'#d33\',
					confirmButtonText: \'Ya, keluar!\'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url: "'.site_url().'auth/do_logout/",
							type: "GET",
							success: function () {
								location.reload();
							}
						});
					}
				});
			}
			</script>';
		return $html;
	}
}
