<?php


class Monitoring_m extends CI_Model{
	public function __construct()
	{
		parent::__construct();
	}

	public function getAll(){
		return $this->db->get('monitoring')->result_array();
	}

	public function getWhere($id){
		return $this->db->get_where('monitoring', array('id' => $id))->row();
	}

	public function getWhereToken($id){
		return $this->db
			->select("*")
			->from("monitoring")
			->where("id_token", $id)
			->order_by("tgl_update", "desc")
			->get()->result_array();
	}

	public function getCount(){
		return $this->db->count_all('monitoring');
	}

	public function save($data){
		return $this->db->insert('monitoring', $data);
	}

	public function edit($id, $data){
		return $this->db->update('monitoring', $data, array('id' => $id));
	}

	public function hapus($id){
		return $this->db->delete('monitoring', array('id' => $id));
	}
}
