<?php


class Sqliteprocess_m extends CI_Model{
	public function __construct()
	{
		parent::__construct();
	}
	public function action($file, $act){
		$mydb = $this->connectSQLITE($file);
		if ($act == "list_attacks"){
			$list = $this->list_attack();
			$dtdanger = array();
			$ctdanger = 0;
			$dtwarning = array();
			$ctwarning = 0;
			$dtresult = array();
			for ($i=0;$i<count($list['danger']);$i++){
				$exe = $mydb->select("dir, scan")->like("scan", $list['danger'][$i])->get("first_log")->result_array();
				$ctdanger += count($exe);
				$dtdanger[$list['danger'][$i]] = count($exe);
			}
			$dtdanger['count'] = $ctdanger;
			for ($i=0;$i<count($list['warning']);$i++){
				$exe = $mydb->select("dir, scan")->like("scan", $list['warning'][$i])->get("first_log")->result_array();
				$ctwarning += count($exe);
				$dtwarning[$list['warning'][$i]] = count($exe);
			}
			$dtwarning['count'] = $ctwarning;
			$exe = $mydb->get("first_log")->num_rows();
			$dtresult['danger'] = $dtdanger;
			$dtresult['warning'] = $dtwarning;
			$dtresult['covered'] = $exe;
			return $dtresult;
		}elseif ($act == "list"){
			$table = $mydb->get("first_log")->result_array();
			return $table;
		}else{
			return array("message"=>"action not found");
		}
	}

	public function count_detector(){
		$this->load->model("Targetmon_m");
		$datas = $this->Targetmon_m->getAllno();
		$list = $this->list_scanner();
		$dtresult = array();
		$dtresult['clean_code'] = 0;
		$dtresult['all_threat'] = 0;
		for($i=0;$i<count($list['lists']);$i++){ $dtresult[$list['lists'][$i]] = 0; }
		foreach ($datas as $data){
			$mydb = $this->connectSQLITE($data['file_init']);
			for($i=0;$i<count($list['lists']);$i++){
				$exe = $mydb->select("dir, scan")->like("scan", $list['lists'][$i])->get("first_log")->num_rows();
				$dtresult[$list['lists'][$i]] += $exe;
				$dtresult['all_threat'] += $exe;
			}
			$exe = $mydb->select("dir, scan")->where("scan", "-")->get("first_log")->num_rows();
			$dtresult['clean_code'] += $exe;
		}
		return $dtresult;
	}

	public function getDetectionWhere($file){
		$list = $this->list_scanner();
		$dtresult = array();
		$dtresult['clean_code'] = 0;
		$dtresult['all_threat'] = 0;
		for($i=0;$i<count($list['lists']);$i++){ $dtresult[$list['lists'][$i]] = 0; }
			$mydb = $this->connectSQLITE($file);
			for($i=0;$i<count($list['lists']);$i++){
				$exe = $mydb->select("dir, scan")->like("scan", $list['lists'][$i])->get("first_log")->num_rows();
				$dtresult[$list['lists'][$i]] += $exe;
				$dtresult['all_threat'] += $exe;
			}
			$exe = $mydb->select("dir, scan")->where("scan", "-")->get("first_log")->num_rows();
			$dtresult['clean_code'] += $exe;
		return $dtresult;
	}

	public function many_change(){
		return $this->db
			->select("target_monitoring.id, target_monitoring.link_apps, target_monitoring.label_target, COUNT(monitoring.filename) as many_change, count(monitoring.ignore_file) as cf")
			->from("target_monitoring")
			->join("monitoring", "monitoring.id_token=target_monitoring.id")
			->limit(5)
			->order_by("monitoring.tgl_update", "desc")
			->get()->result_array();
	}

	public function latest_change(){
		return $this->db
			->select("monitoring.filename, monitoring.scan_detector, monitoring.mode_file, target_monitoring.label_target")
			->from("target_monitoring")
			->join("monitoring", "monitoring.id_token=target_monitoring.id")
			->limit(10)
			->order_by("monitoring.tgl_update", "desc")
			->get()->result_array();
	}

	public function count_ignore(){
		return $this->db
			->select("count(monitoring.ignore_file) as cf, count(monitoring.filename) as all_file")
			->from("target_monitoring")
			->join("monitoring", "monitoring.id_token=target_monitoring.id")
			->get()->row();
	}

	public function list_attack(){
		$bk = file_get_contents("./list_attacks.json");
		$pars = json_decode($bk, true);
		return $pars;
	}

	public function list_scanner(){
		$bk = file_get_contents("./list_scanner.json");
		$pars = json_decode($bk, true);
		return $pars;
	}

	public function connectSQLITE($data){
		$config['hostname'] = "";
		$config['username'] = "";
		$config['password'] = "";
		$config['database'] = "./tmp_file/".$data;
		$config['dbdriver'] = "sqlite3";
		$config['dbprefix'] = "";
		$config['pconnect'] = FALSE;
		$config['db_debug'] = (ENVIRONMENT !== 'production');
		$config['cache_on'] = FALSE;
		$config['cachedir'] = "";
		$config['char_set'] = "utf8";
		$config['dbcollat'] = "utf8_general_ci";
		$config['swap_pre'] = '';
		$config['encrypt'] = FALSE;
		$config['compress'] = FALSE;
		$config['stricton'] = FALSE;
		$config['failover'] = array();
		$config['save_queries'] = TRUE;

		$db = $this->load->database($config, TRUE);
		return $db;
	}
}
