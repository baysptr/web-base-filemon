<?php


class Grouptelegram_m extends CI_Model{
	public function __construct()
	{
		parent::__construct();
	}

	public function getAll(){
		return $this->db->get('group_telegram')->result_array();
	}

	public function getWhere($id){
		return $this->db->get_where('group_telegram', array('id' => $id))->row();
	}

	public function getCount(){
		return $this->db->count_all('group_telegram');
	}

	public function save($data){
		return $this->db->insert('group_telegram', $data);
	}

	public function edit($id, $data){
		return $this->db->update('group_telegram', $data, array('id' => $id));
	}

	public function hapus($id){
		return $this->db->delete('group_telegram', array('id' => $id));
	}
}
