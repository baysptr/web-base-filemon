<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		if(!isset($this->session->userdata['level'])) {
			redirect(site_url()."auth");
		}else{
			redirect(site_url()."dashboard");
		}
	}
}
