<?php

date_default_timezone_set("Asia/Jakarta");

class Manage_target extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->userdata['level'])) {
			echo "<script>alert('Maaf credentials anda tidak kami ketahui!');window.location='" . site_url() . "auth';</script>";
		}
		$this->load->model("Layout_m");
		$this->load->model("Userlog_m");
		$this->load->model("Targetmon_m");
		$this->load->model("Grouptelegram_m");
	}
	public function index(){
		$data['head'] = $this->Layout_m->head('Manage Target');
		$data['header'] = $this->Layout_m->header('manage_target');
		$data['footer'] = $this->Layout_m->footer();
		$data['javascript'] = $this->Layout_m->javascript();
		$data['datas'] = $this->Targetmon_m->getAll();
		$this->load->view("manage_target", $data);
	}

	public function on_change($status){
		if($status == "group"){
			$datas = $this->Grouptelegram_m->getAll();
			$data = "<option>-- Pilih Group --</option>";
			foreach ($datas as $dt){
				$data = $data . "<option value='".$dt['id_group_telegram']."'>".$dt['label_group']."</option>";
			}
			echo $data;
		}else{
			$datas = $this->Userlog_m->getAll();
			$data = "<option>-- Pilih User --</option>";
			foreach ($datas as $dt){
				$data = $data . "<option value='".$dt['id_telegram']."'>".$dt['nama']."</option>";
			}
			echo $data;
		}
	}

	function generateRandomString($length) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#~$%&*?\/';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function do_where($id){
		$data = $this->Targetmon_m->getWhere($id);
		echo json_encode($data);
	}

	public function do_manage_target(){
		$id = $this->input->post("id");
		$data = array(
			"token" => $this->generateRandomString(12),
			"id_telegram" => $this->input->post("gou"),
			"status" => $this->input->post("status"),
			"label_target" => $this->input->post("nama"),
			"link_apps" => $this->input->post("link"),
			"description" => $this->input->post("deskripsi"),
			"directory" => $this->input->post("folder"),
			"id_create_user" => $this->session->userdata("id"),
			"tgl_update" => date("Y-m-d H:i:s")
		);
		if($id){
			if($this->Targetmon_m->edit($id, $data)){
				echo "success";
			}else{
				echo "gagal";
			}
		}else{
			if($this->Targetmon_m->save($data)){
				echo "success";
			}else{
				echo "gagal";
			}
		}
	}

	function do_delete($id){
		if($this->Targetmon_m->hapus($id)){
			echo "success";
		}else{
			echo "gagal";
		}
	}
	public function send_token($id){
		$datas = $this->Targetmon_m->getWhere($id);
		$telegram = $datas->id_telegram;
		$data = "Target: <strong>".$datas->label_target."</strong>
Directory: <strong>".$datas->directory."</strong>

Token ini untuk install bot pada server
<strong>".$datas->token."</strong>";

		$url = "https://api.telegram.org/bot1434855310:AAHYjuMTx5yCAdr58BpMYeNkVA3iMufabfw/sendMessage?parse_mode=html&chat_id=".ltrim($telegram);
		$url = $url . "&text=".urlencode($data);
		$ch = curl_init();
		$optArray = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		);
		curl_setopt_array($ch, $optArray);
		$result = curl_exec($ch);
		curl_close($ch);
		echo "<script>alert('Tunggu 3 - 5 menit untuk mendapatkan pesan dari bot, jika tidak maka ada yang salah dalam id telegram anda, pastikan kembali dengan benar');window.location='".site_url()."manage_target';</script>";
	}
}
