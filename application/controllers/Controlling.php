<?php

date_default_timezone_set("Asia/Jakarta");

class Controlling extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->userdata['level'])) {
			echo "<script>alert('Maaf credentials anda tidak kami ketahui!');window.location='" . site_url() . "auth';</script>";
		}
		$this->load->model("Layout_m");
		$this->load->model("Sqliteprocess_m");
		$this->load->model("Targetmon_m");
		$this->load->model("Monitoring_m");
	}
	public function target(){
		$id = ($this->input->get("id"))?$this->input->get("id"):0;
		if($id != 0){
			$target = $this->Targetmon_m->getWhere($id);

			$data['head'] = $this->Layout_m->head('Monitoring');
			$data['header'] = $this->Layout_m->header('manage_target');
			$data['footer'] = $this->Layout_m->footer();
			$data['javascript'] = $this->Layout_m->javascript();
			$data['logs'] = $this->Sqliteprocess_m->action($target->file_init, "list_attacks");
			$data['monitorings'] = $this->Monitoring_m->getWhereToken($id);
			$data['id'] = $id;
			$this->load->view("monitoring", $data);
		}else{
			echo "<script>alert('Maaf data target tidak kami ketahui');window.location='".site_url()."manage_target';</script>";
		}
	}

	public function get_where($id){
		$data = $this->Monitoring_m->getWhere($id);
		echo json_encode($data);
	}

	public function do_ignore(){
		$id = $this->input->post("id");
		$data = array(
			"ignore_file" => $this->input->post("ignorefile"),
			"tgl_update" => date("Y-m-d H:i:s")
		);
		if ($this->Monitoring_m->edit($id, $data)){
			echo "success";
		}else{
			echo "gagal";
		}
	}

	public function logs(){
		$id = ($this->input->get("id"))?$this->input->get("id"):0;
		if($id != 0){
			$target = $this->Targetmon_m->getWhere($id);

			$data['head'] = $this->Layout_m->head('Logs');
			$data['header'] = $this->Layout_m->header('manage_target');
			$data['footer'] = $this->Layout_m->footer();
			$data['javascript'] = $this->Layout_m->javascript();
			$data['logs'] = $this->Sqliteprocess_m->action($target->file_init, "list");
			$data['id'] = $id;
			$this->load->view("logs", $data);
		}else{
			echo "<script>alert('Maaf data target tidak kami ketahui');window.location='".site_url()."manage_target';</script>";
		}
	}
}
