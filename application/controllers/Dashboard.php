<?php


class Dashboard extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if(!isset($this->session->userdata['level'])) {
			echo "<script>alert('Maaf credentials anda tidak kami ketahui!');window.location='" . site_url() . "auth';</script>";
		}
		$this->load->model("Layout_m");
		$this->load->model("Sqliteprocess_m");
	}

	public function index(){
		$data['head'] = $this->Layout_m->head('Dashboard');
		$data['header'] = $this->Layout_m->header('dashboard');
		$data['footer'] = $this->Layout_m->footer();
		$data['javascript'] = $this->Layout_m->javascript();
		$data['count_detector'] = $this->Sqliteprocess_m->count_detector();
		$data['changes'] = $this->Sqliteprocess_m->many_change();
		$data['ignore'] = $this->Sqliteprocess_m->count_ignore();
		$data['latests'] = $this->Sqliteprocess_m->latest_change();
		$this->load->view("dashboard", $data);
	}
}
