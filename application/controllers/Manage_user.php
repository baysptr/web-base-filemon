<?php

date_default_timezone_set("Asia/Jakarta");

class Manage_user extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->userdata['level'])){
			echo "<script>alert('Maaf credentials anda tidak kami ketahui!');window.location='".site_url()."auth';</script>";
		}elseif ($this->session->userdata('level') != "admin"){
			echo "<script>alert('Maaf, anda tidak bisa akses ini, karena hanya admin!');window.location='".site_url()."dashboard';</script>";
		}
		$this->load->model("Layout_m");
		$this->load->model("Userlog_m");
	}
	public function index(){
		$data['head'] = $this->Layout_m->head('Manage User');
		$data['header'] = $this->Layout_m->header('manage_user');
		$data['footer'] = $this->Layout_m->footer();
		$data['javascript'] = $this->Layout_m->javascript();
		$data['datas'] = $this->Userlog_m->getAll();
		$this->load->view("manage_user", $data);
	}

	public function do_manage_user(){
		$id = $this->input->post("id");
		$data = array(
			"nik" => $this->input->post('nik'),
			"nama" => strtoupper($this->input->post('nama')),
			"alamat" => strtoupper($this->input->post('alamat')),
			"no_telp" => $this->input->post('no_telp'),
			"email" => $this->input->post('email'),
			"jabatan" => strtoupper($this->input->post('jabatan')),
			"id_telegram" => $this->input->post('telegram'),
			"level" => $this->input->post('level'),
			"password" => md5($this->input->post('pass')),
			"tgl_update" => date("Y-m-d H:i:s")
		);
		if($id){
			if($this->Userlog_m->edit($id, $data)){
				echo "success";
			}else{
				echo "gagal";
			}
		}else{
			if($this->Userlog_m->save($data)){
				echo "success";
			}else{
				echo "gagal";
			}
		}
	}

	function do_where($id){
		$data = $this->Userlog_m->getWhere($id);
		echo json_encode($data);
	}

	function do_delete($id){
		if($this->Userlog_m->hapus($id)){
			echo "success";
		}else{
			echo "gagal";
		}
	}
}
