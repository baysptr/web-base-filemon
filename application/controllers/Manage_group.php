<?php

date_default_timezone_set("Asia/Jakarta");

class Manage_group extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->userdata['level'])){
			echo "<script>alert('Maaf credentials anda tidak kami ketahui!');window.location='".site_url()."auth';</script>";
		}
		$this->load->model("Layout_m");
		$this->load->model("Userlog_m");
		$this->load->model("Grouptelegram_m");
	}
	public function index(){
		$data['head'] = $this->Layout_m->head('Manage User');
		$data['header'] = $this->Layout_m->header('manage_group');
		$data['footer'] = $this->Layout_m->footer();
		$data['javascript'] = $this->Layout_m->javascript();
		$data['users'] = $this->Userlog_m->getAll();
		$ori_data = $this->Grouptelegram_m->getAll();
		for ($i=0;$i<count($ori_data);$i++){
			$arr_user = explode(",", $ori_data[$i]['arr_id_user']);
			$data_user = array();
			for($j=0;$j<count($arr_user);$j++){
				$user = $this->Userlog_m->getWhere($arr_user[$j]);
				$mydata = array("id_user" => $user->id, "nama" => $user->nama);
				array_push($data_user, $mydata);
			}
			$ori_data[$i]['arr_id_user'] = $data_user;
			$c_user = $this->Userlog_m->getWhere($ori_data[$i]['id_create_user']);
			$ori_data[$i]['id_create_user'] = array("id" => $c_user->id, "nama" => $c_user->nama);
		}
		$data['groups'] = $ori_data;
		$this->load->view("manage_group", $data);
	}
//	public function test(){
//		$ori_data = $this->Grouptelegram_m->getAll();
//		for ($i=0;$i<count($ori_data);$i++){
//			$arr_user = explode(",", $ori_data[$i]['arr_id_user']);
//			$data_user = array();
//			for($j=0;$j<count($arr_user);$j++){
//				$user = $this->Userlog_m->getWhere($arr_user[$j]);
//				$mydata = array("id_user" => $user->id, "nama" => $user->nama);
//				array_push($data_user, $mydata);
//			}
//			$ori_data[$i]['arr_id_user'] = $data_user;
//			$c_user = $this->Userlog_m->getWhere($ori_data[$i]['id_create_user']);
//			$ori_data[$i]['id_create_user'] = array("id" => $c_user->id, "nama" => $c_user->nama);
//		}
//		print_r($ori_data);
//	}
	public function do_insert(){
		$data = array(
			"id_group_telegram" => $this->input->post('id_group'),
			"label_group" => $this->input->post('nama'),
			"arr_id_user" => implode(",", $this->input->post('anggota')),
			"id_create_user" => $this->session->userdata('id'),
			"tgl_update" => date("Y-m-d H:i:s")
		);
		if($this->Grouptelegram_m->save($data)){
			echo "<script>alert('Group Berhasil dibuat!');window.location='".site_url()."manage_group';</script>";
		}else{
			echo "<script>alert('Group Gagal dibuat, segera hubungi super admin!');window.location='".site_url()."manage_group';</script>";
		}
	}
	public function do_where($id){
		$data = $this->Grouptelegram_m->getWhere($id);
		echo json_encode($data);
	}
	public function do_update(){
		$id = $this->input->post("id");
		$data = array(
			"id_group_telegram" => $this->input->post('edt_id_group'),
			"label_group" => $this->input->post('edt_nama'),
			"arr_id_user" => implode(",", $this->input->post('edt_anggota')),
			"id_create_user" => $this->session->userdata('id'),
			"tgl_update" => date("Y-m-d H:i:s")
		);
		if($this->Grouptelegram_m->edit($id, $data)){
			echo "<script>alert('Group Berhasil diuabh!');window.location='".site_url()."manage_group';</script>";
		}else{
			echo "<script>alert('Group Gagal diubah, segera hubungi super admin!');window.location='".site_url()."manage_group';</script>";
		}
	}
	public function do_delete($id){
		$this->Grouptelegram_m->hapus($id);
		echo "<script>alert('Group Berhasil dihapus!');window.location='".site_url()."manage_group';</script>";
	}
	public function test_bot($id){
		$url = "https://api.telegram.org/bot1434855310:AAHYjuMTx5yCAdr58BpMYeNkVA3iMufabfw/sendMessage?parse_mode=html&chat_id=".$id;
		$url = $url . "&text=".urlencode('Pesan ini dari bot yang sudah anda tambahkan jadi teman');
		$ch = curl_init();
		$optArray = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		);
		curl_setopt_array($ch, $optArray);
		$result = curl_exec($ch);
		curl_close($ch);
		echo "<script>alert('Tunggu 3 - 5 menit untuk mendapatkan pesan dari bot, jika tidak maka ada yang salah dalam id telegram anda, pastikan kembali dengan benar');window.location='".site_url()."manage_group';</script>";
	}

	public function test(){
		$url = "https://api.telegram.org/bot1434855310:AAHYjuMTx5yCAdr58BpMYeNkVA3iMufabfw/ChatMember";
//		$url = $url . "&text=".urlencode('Pesan ini dari bot yang sudah anda tambahkan jadi teman');
		$ch = curl_init();
		$optArray = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		);
		curl_setopt_array($ch, $optArray);
		$result = curl_exec($ch);
		curl_close($ch);
		print_r($result);
	}
}
