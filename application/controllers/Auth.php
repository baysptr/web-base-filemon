<?php


class Auth extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Userlog_m");
	}
	public function index(){
		$this->load->view('login');
	}

	public function do_login(){
		$email = strtoupper($this->input->post('email'));
		$pass = md5($this->input->post('pass'));
		$res = $this->Userlog_m->auth($email, $pass);
		if($res->num_rows() > 0){
			$datas = $res->row();
			$data_session = array(
				"id" => $datas->id,
				"nama" => $datas->nama,
				"level" => $datas->level,
				"jabatan" => $datas->jabatan,
				"logged_in" => true
			);
			$this->session->set_userdata($data_session);
			redirect(site_url().'dashboard');
		}else{
			echo "<script>alert('Maaf data anda tidak kami ketahui!');window.location='".site_url()."auth'</script>";
		}

	}

	public function do_logout(){
		$this->session->sess_destroy();
		redirect(site_url()."auth");
	}
}
