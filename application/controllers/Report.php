<?php


class Report extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		if(!isset($this->session->userdata['level'])) {
			echo "<script>alert('Maaf credentials anda tidak kami ketahui!');window.location='" . site_url() . "auth';</script>";
		}
		$this->load->model("Layout_m");
		$this->load->model("Targetmon_m");
		$this->load->model("Sqliteprocess_m");
		$this->load->model("Monitoring_m");
	}

	public function index(){
		$data['head'] = $this->Layout_m->head('Report');
		$data['header'] = $this->Layout_m->header('report');
		$data['footer'] = $this->Layout_m->footer();
		$data['javascript'] = $this->Layout_m->javascript();
		$data['targets'] = $this->Targetmon_m->getAll();
		$this->load->view("laporan", $data);
	}

	public function do_download(){
		$status = $this->input->post('pilih');
		if($status){
			if($status == "all"){
				$all = $this->Targetmon_m->getAllno();
				for ($i=0;$i<count($all);$i++){
					$all[$i]['scan'] = $this->Sqliteprocess_m->getDetectionWhere($all[$i]['file_init']);
					$all[$i]['monitorings'] = $this->Monitoring_m->getWhereToken($all[$i]['id']);
				}
				$data['all'] = $all;
				$name = "Report_all_".time().".pdf";
				$mpdf = new \Mpdf\Mpdf();
				$html = $this->load->view('pdf_output_all',$data,true);
				$mpdf->WriteHTML($html);
				$mpdf->Output($name,'D');
				redirect(site_url()."report");
			}else{
				$check = $this->Targetmon_m->getCheck($status);
				if($check > 0){
					$i = $this->Targetmon_m->getWhere($status);
					$name = "Report_".time().".pdf";
					$data['target'] = $i;
					$data['scan'] = $this->Sqliteprocess_m->getDetectionWhere($i->file_init);
					$data['monitorings'] = $this->Monitoring_m->getWhereToken($i->id);

					$mpdf = new \Mpdf\Mpdf();
					$html = $this->load->view('pdf_output',$data,true);
					$mpdf->WriteHTML($html);
					$mpdf->Output($name,'D');
					redirect(site_url()."report");
				}else{
					echo "<script>alert('Maaf, nilai yang anda sertakan tidak ada pada sistem kami');window.location='".site_url()."report';</script>";
				}
			}
		}else{
			echo "<script>alert('Maaf, tidak ada nilai yang sistem tangkap');window.location='".site_url()."report';</script>";
		}
	}
}
