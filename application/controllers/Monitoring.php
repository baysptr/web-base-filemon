<?php

date_default_timezone_set("Asia/Jakarta");

class Monitoring extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Targetmon_m");
		$this->load->model("Monitoring_m");
	}
	public function verifikasi_token(){
		$token = $this->input->post("token");
		$datas = $this->Targetmon_m->getWhereToken($token);
		$data = $datas->row();
		if($datas->num_rows() > 0){
			echo json_encode(array("id" => $data->id, "folder" => $data->directory));
		}else{
			echo json_encode(array("id" => 0));
		}
	}

	public function push_init()
	{
		$config['upload_path'] = './tmp_file/';
		$config['allowed_types'] = '*';
		$config['encrypt_name'] = TRUE;
		$config['overwrite'] = true;
		$config['max_size'] = 4096; // 4MB
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('myfile')) {
			$data = array(
				"ip_dns" => $this->input->post("myip"),
				"file_init" => $this->upload->data("file_name")
			);
			if($this->Targetmon_m->edit($this->input->post('id'), $data)){
				echo 1;
			}else{
				echo 0;
			}
		} else {
			echo 0;
		}
		$this->send_token($this->input->post('id'));
	}

	public function push_change(){
		$data = array(
			"id_token" => $this->input->post("id"),
			"mode_file" => $this->input->post('mode'),
			"filename" => $this->input->post('filename'),
			"scan_detector" => $this->input->post('scan'),
			"encryption" => $this->input->post('enkripsi'),
			"tgl_update" => date("Y-m-d H:i:s")
		);
		if($this->Monitoring_m->save($data)){
			echo 1;
		}else{
			echo 0;
		}
		$this->send_notifikasi($data);
	}

	public function send_token($id){
		$datas = $this->Targetmon_m->getWhere($id);
		$telegram = $datas->id_telegram;
		$data = "<strong>".$datas->label_target."</strong>

Anda telah berhasil melakukan scan pertama pada aplikasi ini, silahkan kunjungi web sistem untuk check logs pertama. <strong>Selanjutnya, Sistem Monitoring telah aktif</strong>";

		$url = "https://api.telegram.org/bot1434855310:AAHYjuMTx5yCAdr58BpMYeNkVA3iMufabfw/sendMessage?parse_mode=html&chat_id=".ltrim($telegram);
		$url = $url . "&text=".urlencode($data);
		$ch = curl_init();
		$optArray = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		);
		curl_setopt_array($ch, $optArray);
		$result = curl_exec($ch);
		curl_close($ch);
	}

	public function send_notifikasi($dt){
		$datas = $this->Targetmon_m->getWhere($dt['id_token']);
		$jenis = '';
		if($dt['mode_file'] == "M"){
			$jenis = "MODIFIED";
		}elseif($dt['mode_file'] == "C"){
			$jenis = "CREATED";
		}elseif($dt['mode_file'] == "d"){
			$jenis = "DELETED";
		}
		$telegram = $datas->id_telegram;
		$data = "<strong>".$jenis."</strong>

Aplikasi : <strong>".$datas->label_target."</strong>
File: <strong>".$dt['filename']."</strong>
Encrypt: ".$dt['encryption']."
Malicious: ".$dt['scan_detector'];

		$url = "https://api.telegram.org/bot1434855310:AAHYjuMTx5yCAdr58BpMYeNkVA3iMufabfw/sendMessage?parse_mode=html&chat_id=".ltrim($telegram);
		$url = $url . "&text=".urlencode($data);
		$ch = curl_init();
		$optArray = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		);
		curl_setopt_array($ch, $optArray);
		$result = curl_exec($ch);
		curl_close($ch);
	}
}
