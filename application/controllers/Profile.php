<?php

date_default_timezone_set("Asia/Jakarta");

class Profile extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->userdata['level'])){
			echo "<script>alert('Maaf credentials anda tidak kami ketahui!');window.location='".site_url()."auth';</script>";
		}
		$this->load->model("Layout_m");
		$this->load->model("Userlog_m");
	}
	public function index(){
		$data['head'] = $this->Layout_m->head('Profile');
		$data['header'] = $this->Layout_m->header('profile');
		$data['footer'] = $this->Layout_m->footer();
		$data['javascript'] = $this->Layout_m->javascript();
		$data['user'] = $this->Userlog_m->getWhere($this->session->userdata('id'));
		$this->load->view("profile", $data);
	}

	public function do_general(){
		$id = $this->input->post("id");
		$data = array(
			"nik" => $this->input->post('nik'),
			"nama" => strtoupper($this->input->post('nama')),
			"alamat" => strtoupper($this->input->post('alamat')),
			"no_telp" => $this->input->post('no_telp'),
			"email" => $this->input->post('email'),
			"id_telegram" => $this->input->post('telegram'),
			"tgl_update" => date("Y-m-d H:i:s")
		);
		if($this->Userlog_m->edit($id, $data)){
			echo "<script>alert('Berhasil ubah profile!');window.location='".site_url()."profile';</script>";
		}else{
			echo "<script>alert('Gagal ubah profile, segera hubungi super admin!');window.location='".site_url()."profile';</script>";
		}
	}

	public function do_password(){
		$old = $this->input->post('old');
		$pass = $this->input->post('password');
		$con_pass = $this->input->post('con_password');
		$check = $this->Userlog_m->getWherePass(md5($old));
		if($check->num_rows() > 0){
			if($pass != $con_pass){
				echo "<script>alert('Maaf password baru anda tidak sama!');window.location='". site_url() ."profile'</script>";
			}else{
				$data = array(
					"password" => md5($pass)
				);
				if($this->Userlog_m->edit($this->session->userdata("id"), $data)){
					echo "<script>alert('Berhasil ubah password!');window.location='".site_url()."profile';</script>";
				}else{
					echo "<script>alert('Gagal ubah password, segera hubungi super admin!');window.location='".site_url()."profile';</script>";
				}
			}
		}else{
			echo "<script>alert('Maaf Password lama anda tidak kami temukan!');window.location='". site_url() ."profile'</script>";
		}
	}

	public function test_bot(){
		$da = $this->Userlog_m->getWhere($this->session->userdata("id"));
		$url = "https://api.telegram.org/bot1434855310:AAHYjuMTx5yCAdr58BpMYeNkVA3iMufabfw/sendMessage?parse_mode=html&chat_id=".$da->id_telegram;
		$url = $url . "&text=".urlencode('Pesan ini dari bot yang sudah anda tambahkan jadi teman');
		$ch = curl_init();
		$optArray = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true
		);
		curl_setopt_array($ch, $optArray);
		$result = curl_exec($ch);
		curl_close($ch);
		echo "<script>alert('Tunggu 3 - 5 menit untuk mendapatkan pesan dari bot, jika tidak maka ada yang salah dalam id telegram anda, pastikan kembali dengan benar');window.location='".site_url()."profile';</script>";
	}
}
