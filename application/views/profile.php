<html class="loading" lang="en" data-textdirection="ltr">
	<head>
		<?= $head ?>
	</head>
	<body class="horizontal-layout horizontal-menu navbar-sticky 2-columns footer-static" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
		<?= $header ?>

		<!-- BEGIN: Content-->
		<div class="app-content content">
			<div class="content-overlay"></div>
			<div class="content-wrapper">
				<div class="content-body"><!-- account setting page start -->
					<section id="page-account-settings">
						<div class="row">
							<div class="col-12">
								<div class="row">
									<!-- left menu section -->
									<div class="col-md-3 mb-2 mb-md-0 pills-stacked">
										<ul class="nav nav-pills flex-column">
											<li class="nav-item">
												<a class="nav-link d-flex align-items-center active" id="account-pill-general" data-toggle="pill"
												   href="#account-vertical-general" aria-expanded="true">
													<i class="bx bx-cog"></i>
													<span>General</span>
												</a>
											</li>
											<li class="nav-item">
												<a class="nav-link d-flex align-items-center" id="account-pill-password" data-toggle="pill"
												   href="#account-vertical-password" aria-expanded="false">
													<i class="bx bx-lock"></i>
													<span>Change Password</span>
												</a>
											</li>
											<li class="nav-item">
												<a class="nav-link d-flex align-items-center" id="account-pill-info" data-toggle="pill"
												   href="#account-vertical-info" aria-expanded="false">
													<i class="bx bx-info-circle"></i>
													<span>Info</span>
												</a>
											</li>
										</ul>
									</div>
									<!-- right content section -->
									<div class="col-md-9">
										<div class="card">
											<div class="card-content">
												<div class="card-body">
													<div class="tab-content">
														<div role="tabpanel" class="tab-pane active" id="account-vertical-general"
															 aria-labelledby="account-pill-general" aria-expanded="true">
															<form novalidate action="<?= site_url() ?>profile/do_general" method="post">
																<input type="hidden" name="id" id="id" value="<?= $user->id ?>">
																<div class="row">
																	<div class="col-md-6 col-sm-12">
																		<div class="form-group">
																			<div class="controls">
																				<label>NIK</label>
																				<input type="text" name="nik" id="nik" class="form-control" value="<?= $user->nik ?>">
																			</div>
																		</div>
																	</div>
																	<div class="col-md-6 col-sm-12">
																		<div class="form-group">
																			<div class="controls">
																				<label>NAMA</label>
																				<input type="text" name="nama" id="nama" class="form-control" value="<?= $user->nama ?>">
																			</div>
																		</div>
																	</div>
																	<div class="col-md-12 col-sm-12">
																		<div class="form-group">
																			<div class="controls">
																				<label>ALAMAT</label>
																				<input type="text" name="alamat" id="alamat" class="form-control" value="<?= $user->alamat ?>">
																			</div>
																		</div>
																	</div>
																	<div class="col-md-6 col-sm-12">
																		<div class="form-group">
																			<div class="controls">
																				<label>NO. TELP</label>
																				<input type="text" name="no_telp" id="no_telp" class="form-control" value="<?= $user->no_telp ?>">
																			</div>
																		</div>
																	</div>
																	<div class="col-md-6 col-sm-12">
																		<div class="form-group">
																			<div class="controls">
																				<label>E-MAIL</label>
																				<input type="email" name="email" id="email" class="form-control" value="<?= $user->email ?>">
																			</div>
																		</div>
																	</div>
																	<div class="col-md-12 col-sm-12">
																		<div class="form-group">
																			<div class="controls">
																				<label>ID - TELEGRAM, <small>Cek info, untuk cari tahu</small></label>
																				<input type="text" name="telegram" id="telegram" class="form-control" value="<?= $user->id_telegram ?>">
																			</div>
																		</div>
																	</div>
																	<div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
																		<button type="submit" class="btn btn-primary glow mr-sm-1 mb-1">Save
																			changes</button>
																	</div>
																</div>
															</form>
														</div>
														<div class="tab-pane fade " id="account-vertical-password" role="tabpanel"
															 aria-labelledby="account-pill-password" aria-expanded="false">
															<form novalidate action="<?= site_url() ?>profile/do_password" method="post">
																<div class="row">
																	<div class="col-12">
																		<div class="form-group">
																			<div class="controls">
																				<label>Old Password</label>
																				<input type="password" name="old" class="form-control" required
																					   placeholder="Old Password"
																					   data-validation-required-message="This old password field is required">
																			</div>
																		</div>
																	</div>
																	<div class="col-12">
																		<div class="form-group">
																			<div class="controls">
																				<label>New Password</label>
																				<input type="password" name="password" class="form-control"
																					   placeholder="New Password" required
																					   data-validation-required-message="The password field is required"
																					   minlength="6">
																			</div>
																		</div>
																	</div>
																	<div class="col-12">
																		<div class="form-group">
																			<div class="controls">
																				<label>Retype new Password</label>
																				<input type="password" name="con_password"
																					   class="form-control" required
																					   data-validation-match-match="password"
																					   placeholder="New Password"
																					   data-validation-required-message="The Confirm password field is required"
																					   minlength="6">
																			</div>
																		</div>
																	</div>
																	<div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
																		<button type="submit" class="btn btn-primary glow mr-sm-1 mb-1">Save
																			changes</button>
																		<button type="reset" class="btn btn-light mb-1">Cancel</button>
																	</div>
																</div>
															</form>
														</div>
														<div class="tab-pane fade" id="account-vertical-info" role="tabpanel" aria-labelledby="account-pill-info" aria-expanded="false">
															<ul class="widget-timeline mb-0">
																<li class="timeline-items timeline-icon-primary active">
																	<div class="timeline-time"><strong>Pertama</strong></div>
																	<h6 class="timeline-title">Tambahkan Bot Filemon ke akun telegram</h6>
																	<div class="timeline-content">
																		<p>Pastikan anda sudah mempunyai akun telegram, ketika sudah punya, ketikan <strong>@mantengin_bot</strong> pada kolom pencarian telegram anda, lalu follow / start. Seperti gambar dibawah berikut.<br/><img src="<?= base_url() ?>assets/images/bot_info.jpg" width="40%" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"></p>
																	</div>
																</li>
																<li class="timeline-items timeline-icon-primary active">
																	<div class="timeline-time"><strong>Kedua</strong></div>
																	<h6 class="timeline-title">Menemukan ID - Telegram anda</h6>
																	<div class="timeline-content">
																		<p>Untuk menemukan ID - Telegram anda, pastikan anda telah mempunyai akun Telegram. Setelah itu silahkan ketikan <strong>@kwkw_bot</strong> pada kolom pencarian telegram anda, lalu klik follow / start, setelah itu nuliskan <strong>!id</strong> untuk meminta id telegram, setelah itu nuliskan di kolom ID - Telegram pada halaman Profile > General Seperti pada gambar berikut.<br/><img src="<?= base_url() ?>assets/images/bot_nama.png" width="40%" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?= base_url() ?>assets/images/id_profile.png" width="40%" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"></p>
																	</div>
																</li>
																<li class="timeline-items timeline-icon-primary active pb-0">
																	<div class="timeline-time"><strong>Terakhir</strong></div>
																	<h6 class="timeline-title">Test Bot, mendapatkan pesan dari Bot</h6>
																	<div class="timeline-content">
																		<p><a href="<?= site_url() ?>profile/test_bot" class="btn btn-sm btn-warning">Test Bot</a>&nbsp;&nbsp;klik tombol tersebut untuk jalankan bot. Lalu pastika anda menerima pesan dari Bot berupa <strong>"Pesan ini dari bot yang sudah anda tambahkan jadi teman"</strong></p>
																	</div>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- account setting page ends -->
				</div>
			</div>
		</div>
		<!-- END: Content-->
		<?= $footer ?>
		<!-- END: Footer-->

		<!-- BEGIN: Vendor JS-->
		<?= $javascript ?>
		<!-- END: Page JS-->
	</body>
</html>
