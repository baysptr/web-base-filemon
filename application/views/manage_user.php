<html class="loading" lang="en" data-textdirection="ltr">
	<head>
		<?= $head ?>
	</head>
	<body class="horizontal-layout horizontal-menu navbar-sticky 2-columns footer-static" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
		<?= $header ?>

		<!-- BEGIN: Content-->
		<div class="app-content content">
			<div class="content-overlay"></div>
			<div class="content-wrapper">
				<div class="content-header row">
				</div>
				<div class="content-body">
					<!-- Dashboard Analytics Start -->
					<section id="dashboard-analytics">
						<div class="row">
							<!-- Website Analytics Starts-->
							<div class="col-md-12 col-sm-12">
								<div class="card">
									<div class="card-header d-flex justify-content-between align-items-center">
										<h4 class="card-title">List User</h4>
									</div>
									<div class="card-content">
										<div class="card-body pb-1">
											<table class="table table-responsive-md">
												<thead>
													<tr>
														<td><strong>NAMA</strong></td>
														<td><strong>NO. TELP</strong></td>
														<td><strong>JABATAN</strong></td>
														<td><strong>LEVEL</strong></td>
														<td><div class="btn btn-block btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Tambah User" onclick="tambahUser()"><span class="ficon bx bxs-add-to-queue"></span></div></td>
													</tr>
												</thead>
												<tbody>
												<?php foreach ($datas as $data){ ?>
													<tr>
														<td><?php echo ($data['id_telegram']==null) ? '<span style="color: red" data-toggle="tooltip" data-placement="top" title="ID Telegram tidak ada" class="ficon bx bxs-check-square"></span> ' . $data['nama'] : '<span style="color: #00CC00" data-toggle="tooltip" data-placement="top" title="ID Telegram ada" class="ficon bx bxs-check-square"></span> ' . $data['nama'] ?></td>
														<td><?= $data['no_telp'] ?></td>
														<td><?= $data['jabatan'] ?></td>
														<td><?= strtoupper($data['level']) ?></td>
														<td><div onclick="detailUser('<?= $data['id'] ?>')" class="btn btn-warning btn-sm btn-block" data-toggle="tooltip" data-placement="top" title="Detail User"><span class="ficon bx bx-detail"></span></div></td>
													</tr>
												<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
		<!-- END: Content-->
		<?= $footer ?>
		<!-- END: Footer-->

		<div class="modal fade text-left w-100" id="userForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="titleForm"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<i class="bx bx-x"></i>
						</button>
					</div>
					<div class="modal-body">
						<form class="form form-vertical" id="myForm">
							<input type="hidden" name="id" id="id">
							<div class="form-body">
								<div class="row">
									<div class="col-md-6 col-12">
										<div class="form-group">
											<label>NIK</label>
											<input type="text" class="form-control" name="nik" id="nik" placeholder="ex: 6075489367458693" onkeypress="javascript:return isNumber(event)">
										</div>
									</div>
									<div class="col-md-6 col-12">
										<div class="form-group">
											<label>NAMA LENGKAP</label>
											<input type="text" class="form-control" name="nama" id="nama" placeholder="ex: Muhammad Sasana Bhakti">
										</div>
									</div>
									<div class="col-12">
										<div class="form-group">
											<label>ALAMAT</label>
											<input type="text" class="form-control" name="alamat" id="alamat" placeholder="ex: Jl. Bahagia No. 19">
										</div>
									</div>
									<div class="col-md-6 col-12">
										<div class="form-group">
											<label>NO. TELP</label>
											<input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="ex: 089677271257" onkeypress="javascript:return isNumber(event)">
										</div>
									</div>
									<div class="col-md-6 col-12">
										<div class="form-group">
											<label>E-MAIL</label>
											<input type="email" class="form-control" name="email" id="email" placeholder="ex: example@mail.com">
										</div>
									</div>
									<div class="col-md-6 col-12">
										<div class="form-group">
											<label>JABATAN</label>
											<input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="ex: SysAdmin, Programmer, DevOps">
										</div>
									</div>
									<div class="col-md-6 col-12">
										<div class="form-group">
											<label>LEVEL USER</label>
											<select name="level" class="form-control" id="level">
												<option selected disabled>-- Pilih Level --</option>
												<option value="admin">ADMIN</option>
												<option value="operator">OPERATOR</option>
												<option value="view">REVIEW</option>
											</select>
										</div>
									</div>
									<div class="col-md-6 col-12">
										<div class="form-group">
											<label>PASSWORD <div class="btn btn-sm btn-warning" onclick="buatPass()" data-toggle="tooltip" data-placement="top" title="Generate Password"><i class="ficon bx bxs-key"></i></div></label>
											<input type="text" class="form-control" name="pass" id="pass" placeholder="note: tulis password atau generate">
										</div>
									</div>
									<div class="col-md-6 col-12">
										<div class="form-group">
											<label>ID TELEGRAM (Optional)</label>
											<input type="text" class="form-control" name="telegram" id="telegram" placeholder="ex: 1134567">
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-light-secondary" id="btnClose" onclick="closeForm()">
							<i class="bx bx-x d-block d-sm-none"></i>
							<span class="d-none d-sm-block">Close</span>
						</button>
						<button type="button" class="btn btn-warning ml-1" id="btnEdit" onclick="actEdit()">
							<i class="bx bx-edit d-block d-sm-none"></i>
							<span class="d-none d-sm-block">Edit</span>
						</button>
						<button type="button" class="btn btn-danger ml-1" id="btnHapus" onclick="actHapus()">
							<i class="bx bx-trash d-block d-sm-none"></i>
							<span class="d-none d-sm-block">Hapus</span>
						</button>
						<button type="button" class="btn btn-primary ml-1" id="btnPush" onclick="pushData()">
							<i class="bx bx-check d-block d-sm-none"></i>
							<span class="d-none d-sm-block">Simpan</span>
						</button>
					</div>
				</div>
			</div>
		</div>

		<!-- BEGIN: Vendor JS-->
		<?= $javascript ?>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
		<!-- END: Page JS-->

		<script>
			function isNumber(evt) {
				var iKeyCode = (evt.which) ? evt.which : evt.keyCode
				if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
					return false;

				return true;
			}
			function genPass(length) {
				var result           = '';
				var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@$%&*!';
				var charactersLength = characters.length;
				for ( var i = 0; i < length; i++ ) {
					result += characters.charAt(Math.floor(Math.random() * charactersLength));
				}
				return result;
			}
			function buatPass() {
				var pass = genPass(14);
				$("#userForm #pass").val(pass);
			}
			function tambahUser() {
				$("#titleForm").html("From Tambah User");
				$("#btnEdit").hide();
				$("#btnHapus").hide();
				$("#btnPush").show();
				activeEdit();
				$("#userForm").modal("show");
			}
			function closeForm() {
				$("#myForm")[0].reset();
				$("#btnEdit").show();
				$("#btnHapus").show();
				$("#btnPush").hide();
				$("#userForm").modal("hide");
			}
			function pushData() {
				$.ajax({
					url: "<?= site_url() ?>manage_user/do_manage_user",
					type: "POST",
					data: $("#myForm").serialize(),
					success: function (data) {
						if(data==="success"){
							location.reload();
						}else{
							closeForm();
							alert("Terjadi masalah, segera hubungi admin");
						}
					}
				})
			}

			function detailUser(id) {
				$.ajax({
					url: "<?= site_url() ?>manage_user/do_where/"+id,
					type: "GET",
					dataType: "JSON",
					success: function (data) {
						$("#titleForm").html("Detail User");
						$("#id").val(data.id);
						$("#nik").val(data.nik);
						$("#nama").val(data.nama);
						$("#alamat").val(data.alamat);
						$("#no_telp").val(data.no_telp);
						$("#email").val(data.email);
						$("#jabatan").val(data.jabatan);
						$("#level").val(data.level);
						$("#pass").val("");
						$("#telegram").val(data.id_telegram);
						$("#btnPush").hide();
						noActiveEdit();
						$("#userForm").modal("show");
					}
				})
			}

			function noActiveEdit() {
				$("#nik").attr("readonly", true);
				$("#nama").attr("readonly", true);
				$("#alamat").attr("readonly", true);
				$("#email").attr("readonly", true);
				$("#no_telp").attr("readonly", true);
				$("#jabatan").attr("readonly", true);
				$("#level").attr("readonly", true);
				$("#pass").attr("readonly", true);
				$("#telegram").attr("readonly", true);
			}

			function activeEdit() {
				$("#nik").attr("readonly", false);
				$("#nama").attr("readonly", false);
				$("#alamat").attr("readonly", false);
				$("#email").attr("readonly", false);
				$("#no_telp").attr("readonly", false);
				$("#jabatan").attr("readonly", false);
				$("#level").attr("readonly", false);
				$("#pass").attr("readonly", false);
				$("#telegram").attr("readonly", false);
			}

			function actEdit() {
				activeEdit();
				$("#btnPush").show();
				$("#btnEdit").hide();
				$("#btnHapus").hide();
			}

			function actHapus() {
				Swal.fire({
					title: 'Apakah anda yakin?',
					text: "Pastikan benar bahwa anda ingin hapus data ini!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, hapus!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url: "<?= site_url() ?>manage_user/do_delete/"+$("#id").val(),
							type: "GET",
							success: function (data) {
								if(data==="success"){
									Swal.fire(
										'Deleted!',
										'Data berhasil dihapus.',
										'success'
									);
									location.reload();
								}else{
									closeForm();
									alert("Terjadi masalah, segera hubungi admin");
								}
							}
						});
					}
				});
			}
		</script>
	</body>
</html>
