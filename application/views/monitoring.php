<html class="loading" lang="en" data-textdirection="ltr">
	<head>
		<?= $head ?>
	</head>
	<body class="horizontal-layout horizontal-menu navbar-sticky 2-columns footer-static" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
		<?= $header ?>

		<!-- BEGIN: Content-->
		<div class="app-content content">
			<div class="content-overlay"></div>
			<div class="content-wrapper">
				<div class="content-header row">
					<div class="content-header-left col-12 mb-2 mt-1">
						<div class="row breadcrumbs-top">
							<div class="col-12">
								<h5 class="content-header-title float-left pr-1 mb-0">Monitoring</h5>
								<div class="breadcrumb-wrapper col-12">
									<ol class="breadcrumb p-0 mb-0">
										<li class="breadcrumb-item"><a href="<?= site_url() ?>manage_target">Control Target</a>
										</li>
										<li class="breadcrumb-item active">Monitoring
										</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="content-body">
					<section>
						<div class="row">
							<div class="col-md-3 col-sm-6">
								<div class="card bg-info">
									<div class="card-header d-flex justify-content-between align-items-center">
										<h4 class="card-title"><strong>COVERED!</strong> all of files</h4>
									</div>
									<div class="card-content">
										<div class="card-body pb-1 text-center">
											<h1><?= $logs['covered'] ?> Files</h1>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="card bg-warning">
									<div class="card-header d-flex justify-content-between align-items-center">
										<h4 class="card-title"><strong>WARNING!</strong> Detected</h4>
									</div>
									<div class="card-content">
										<div class="card-body pb-1 text-center">
											<h1><?= $logs['warning']['count'] ?> Files</h1>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="card bg-danger">
									<div class="card-header d-flex justify-content-between align-items-center">
										<h4 class="card-title"><strong>DANGER!</strong> Detected</h4>
									</div>
									<div class="card-content">
										<div class="card-body pb-1 text-center">
											<h1><?= $logs['danger']['count'] ?> Files</h1>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="card">
									<div class="card-header d-flex justify-content-between align-items-center">
										<h4 class="card-title"><strong>VIEW!</strong> Logs file</h4>
									</div>
									<div class="card-content">
										<div class="card-body pb-1 text-center">
											<a href="<?= site_url() ?>controlling/logs?id=<?= $id ?>" class="btn btn-lg btn-block btn-primary">Lihat</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12">
								<div class="card">
									<div class="card-header d-flex justify-content-between align-items-center">
										<h4 class="card-title"><strong>CONTROLLER!</strong> of monitoring</h4><small>Klik filename, untuk ignore file</small>
									</div>
									<div class="card-content">
										<div class="card-body pb-1">
											<table class="table table-responsive">
												<thead>
													<tr>
														<td><strong>FILE</strong></td>
														<td><strong>MODE</strong></td>
														<td><strong>MALICIOUS</strong></td>
													</tr>
												</thead>
												<tbody>
													<?php foreach ($monitorings as $monitoring){ ?>
														<tr>
															<td>
																<?php if($monitoring['ignore_file']){ ?>
																	<span style="color: #39DA8A" class="ficon bx bx-check-shield" data-toggle="tooltip" data-placement="top" title="File ini sudah diberi komentar atau Ignore!"></span>
																<?php } ?>
																<a onclick="opIgnore('<?= $monitoring['id'] ?>')" style="color: #727E8C" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Klik, untuk memberikan ignore file!"><?= $monitoring['filename'] ?></a>
															</td>
															<td><?php if($monitoring['mode_file'] == "C"){ echo "CREATED"; }elseif ($monitoring['mode_file']=="M"){echo "MODIFIED";}else{echo"DELETED";} ?></td>
															<td><?= $monitoring['scan_detector'] ?></td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
		<!-- END: Content-->
		<?= $footer ?>
		<!-- END: Footer-->

		<div class="modal fade text-left w-100" id="groupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<i class="bx bx-x"></i>
						</button>
					</div>
					<div class="modal-body">
						<form class="form form-vertical" id="formGroup" method="post" action="<?= site_url() ?>controlling/do_ignore">
							<input type="hidden" name="id" id="id">
							<div class="form-body">
								<div class="row">
									<div class="col-12">
										<fieldset class="form-label-group">
											<textarea class="form-control" name="ignorefile" id="ignorefile" rows="5" placeholder="Tuliskan, File ignore disini ....."></textarea>
											<label for="ignorefile">Ignore, this file</label>
										</fieldset>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-light-secondary" id="btnClose" onclick="closeModal()">
							<i class="bx bx-x d-block d-sm-none"></i>
							<span class="d-none d-sm-block">Close</span>
						</button>
						<button type="button" class="btn btn-primary ml-1" id="btnPush" onclick="pushData()">
							<i class="bx bx-check d-block d-sm-none"></i>
							<span class="d-none d-sm-block">Simpan</span>
						</button>
					</div>
				</div>
			</div>
		</div>

		<!-- BEGIN: Vendor JS-->
		<?= $javascript ?>
		<script>
			function closeModal() {
				$("#formGroup")[0].reset();
				$("#groupModal").modal("hide");
			}
			function pushData() {
				$.ajax({
					url: "<?= site_url() ?>controlling/do_ignore",
					type: "POST",
					data: $("#formGroup").serialize(),
					success: function (data) {
						if(data==="success"){
							location.reload();
						}else{
							closeModal();
							alert("Terjadi masalah, segera hubungi admin");
						}
					}
				});
			}
			function opIgnore(id) {
				$.ajax({
					url: "<?= site_url() ?>controlling/get_where/"+id,
					type: "GET",
					dataType: "JSON",
					success: function (data) {
						$(".modal-title").html("File: "+data.filename);
						$("#id").val(data.id);
						$("#ignorefile").val(data.ignore_file);
						$("#groupModal").modal("show");
					}
				})
			}
		</script>
	</body>
</html>
