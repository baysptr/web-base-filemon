<html class="loading" lang="en" data-textdirection="ltr">
	<head>
		<?= $head ?>
	</head>
	<body class="horizontal-layout horizontal-menu navbar-sticky 2-columns footer-static" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
		<?= $header ?>

		<!-- BEGIN: Content-->
		<div class="app-content content">
			<div class="content-overlay"></div>
			<div class="content-wrapper">
				<div class="content-header row">
					<div class="content-header-left col-12 mb-2 mt-1">
						<div class="row breadcrumbs-top">
							<div class="col-12">
								<h5 class="content-header-title float-left pr-1 mb-0">First Logs</h5>
								<div class="breadcrumb-wrapper col-12">
									<ol class="breadcrumb p-0 mb-0">
										<li class="breadcrumb-item"><a href="<?= site_url() ?>manage_target">Control Target</a></li>
										<li class="breadcrumb-item"><a href="<?= site_url() ?>controlling/target?id=<?= $id ?>">Controlling</a></li>
										<li class="breadcrumb-item active">Logs</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="content-body">
					<section>
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="card">
									<div class="card-header d-flex justify-content-between align-items-center">
										<h4 class="card-title"><strong>COVERED!</strong> all of files</h4>
									</div>
									<div class="card-content">
										<div class="card-body pb-1">
											<table class="table table-responsive">
												<thead>
												<tr>
													<td><strong>FILE</strong></td>
													<td><strong>ENCRYPT</strong></td>
													<td><strong>MALICIOUS</strong></td>
												</tr>
												</thead>
												<tbody>
													<?php foreach ($logs as $log){ ?>
														<tr>
															<td><?= $log['dir'] ?></td>
															<td><?= $log['enk'] ?></td>
															<td><?= $log['scan'] ?></td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
		<!-- END: Content-->
		<?= $footer ?>
		<!-- END: Footer-->

		<!-- BEGIN: Vendor JS-->
		<?= $javascript ?>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
		<!-- END: Page JS-->
	</body>
</html>
