<html class="loading" lang="en" data-textdirection="ltr">
	<head>
		<?= $head ?>
		<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
	</head>
	<body class="horizontal-layout horizontal-menu navbar-sticky 2-columns footer-static" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
		<?= $header ?>

		<!-- BEGIN: Content-->
		<div class="app-content content">
			<div class="content-overlay"></div>
			<div class="content-wrapper">
				<div class="content-header row">
				</div>
				<div class="content-body">
					<!-- Dashboard Analytics Start -->
					<section id="dashboard-analytics">
						<div class="row">
							<!-- Website Analytics Starts-->
							<div class="col-md-12 col-sm-12">
								<div class="card">
									<div class="card-header d-flex justify-content-between align-items-center">
										<h4 class="card-title">Reports of Targets</h4>
									</div>
									<div class="card-content">
										<div class="card-body pb-1">
											<form action="<?= site_url() ?>report/do_download" method="post">
												<div class="row">
													<div class="col-md-9 col-sm-12">
														<select name="pilih" id="pilih" class="form-control">
															<option selected disabled>-- Pilih jenis laporan --</option>
															<option value="all">Semua Target</option>
															<?php foreach ($targets as $target){ if ($target['file_init']){ ?>
																<option value="<?= $target['id'] ?>"><?= $target['label_target'] ?></option>
															<?php } } ?>
														</select>
													</div>
													<div class="col-md-3 col-sm-12">
														<button type="submit" class="btn btn-block btn-primary"><span class="ficon bx bxs-cloud-download"></span> Download</button>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
		<!-- END: Content-->
		<?= $footer ?>
		<!-- END: Footer-->

		<!-- BEGIN: Vendor JS-->
		<?= $javascript ?>
		<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#pilih').select2();
			});
		</script>
	</body>
</html>
