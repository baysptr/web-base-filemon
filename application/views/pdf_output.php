<html>
	<head>
		<title>Output Report</title>
		<style>
			body{
				font-size: 12pt;
			}
			body h3{
				font-size: 16pt;
			}
		</style>
	</head>
	<body>
		<table border="1" style="table-layout: fixed; width: 100%; font-size: 12pt">
			<tr><td colspan="4" style="text-align: center"><strong><h3>Target</h3></strong></td></tr>
			<tr>
				<td>Nama Target</td>
				<td><?= $target->label_target ?></td>
				<td>Link Target</td>
				<td><?= $target->link_apps ?></td>
			</tr>
			<tr>
				<td>Deskripsi Target</td>
				<td colspan="3"><?= $target->description ?></td>
			</tr>
			<tr>
				<td>Directory Target</td>
				<td><?= $target->directory ?></td>
				<td>IP Target</td>
				<td><?= $target->ip_dns ?></td>
			</tr>
			<tr><td colspan="4" style="text-align: center"><strong><h3>Covered Files</h3></strong></td></tr>
			<tr>
				<td>Clean Code</td>
				<td><?= $scan['clean_code'] ?></td>
				<td>Password Protection</td>
				<td><?= $scan['PasswordProtection'] ?></td>
			</tr>
			<tr>
				<td>Obfuscated PHP</td>
				<td><?= $scan['ObfuscatedPhp'] ?></td>
				<td>Dangerous PHP</td>
				<td><?= $scan['DangerousPhp'] ?></td>
			</tr>
			<tr>
				<td>Hidden in a File</td>
				<td><?= $scan['HiddenInAFile'] ?></td>
				<td>Cloud Flare Bypass</td>
				<td><?= $scan['CloudFlareBypass'] ?></td>
			</tr>
			<tr>
				<td>Suspicious Encoding</td>
				<td><?= $scan['SuspiciousEncoding'] ?></td>
				<td>Websites Code</td>
				<td><?= $scan['Websites'] ?></td>
			</tr>
			<tr>
				<td>Dodgy PHP</td>
				<td><?= $scan['DodgyPhp'] ?></td>
				<td>Dodgy Strings Code</td>
				<td><?= $scan['DodgyStrings'] ?></td>
			</tr>
			<tr><td colspan="4" style="text-align: center"><strong><h3>Behaviour Files</h3></strong></td></tr>
			<tr>
				<td><strong>FILENAME</strong></td>
				<td><strong>MODE</strong></td>
				<td><strong>DETECTOR</strong></td>
				<td><strong>TANGGAL</strong></td>
			</tr>
			<?php foreach ($monitorings as $monitoring){ ?>
				<tr>
					<td><?= $monitoring['filename'] ?></td>
					<td><?php if($monitoring['mode_file'] == "C"){ echo "CREATED"; }elseif ($monitoring['mode_file']=="M"){echo "MODIFIED";}else{echo"DELETED";} ?></td>
					<td style="word-wrap: break-word"><?= $monitoring['scan_detector'] ?></td>
					<td><?= $monitoring['tgl_update'] ?></td>
				</tr>
			<?php } ?>
		</table>
	</body>
</html>
