<html class="loading" lang="en" data-textdirection="ltr">
	<head>
		<?= $head ?>
	</head>
	<body class="horizontal-layout horizontal-menu navbar-sticky 2-columns footer-static" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
		<?= $header ?>

		<!-- BEGIN: Content-->
		<div class="app-content content">
			<div class="content-overlay"></div>
			<div class="content-wrapper">
				<div class="content-header row">
				</div>
				<div class="content-body">
					<!-- Dashboard Analytics Start -->
					<section id="dashboard-analytics">
						<div class="row">
							<!-- Website Analytics Starts-->
							<div class="col-md-12 col-sm-12">
								<div class="card">
									<div class="card-header d-flex justify-content-between align-items-center">
										<h4 class="card-title">List of Target Monitoring</h4>
									</div>
									<div class="card-content">
										<div class="card-body pb-1">
											<table class="table table-responsive-md">
												<thead>
													<tr>
														<td><strong>NAME</strong></td>
														<td><strong>IP SERVER</strong></td>
														<td><strong>NOTIF STATUS</strong></td>
														<td colspan="3"><div onclick="tambahUser()" class="btn btn-sm btn-block btn-primary" data-toggle="tooltip" data-placement="top" title="Tambah Target Monitoring"><span class="ficon bx bxs-add-to-queue"></span></div></td>
													</tr>
												</thead>
												<tbody>
													<?php foreach ($datas as $data){ ?>
														<tr>
															<td><a target="_blank" href="<?= $data['link_apps'] ?>"><?= $data['label_target'] ?></a></td>
															<td><?= ($data['ip_dns']==null) ? '<span class="bg-dark text-white">Bot not active</span>' : $data['ip_dns'] ?></td>
															<td><?= strtoupper($data['status']) ?></td>
															<td><div onclick="detailUser('<?= $data['id'] ?>')" class="btn btn-sm btn-block btn-warning" data-toggle="tooltip" data-placement="top" title="Detail Target Monitoring"><span class="ficon bx bxs-detail"></span></div></td>
															<td><div onclick="window.location='<?= site_url() ?>manage_target/send_token/<?= $data['id'] ?>'" class="btn btn-sm btn-block btn-info" data-toggle="tooltip" data-placement="top" title="Request token target ini!"><span class="ficon bx bxs-barcode"></span></div></td>
															<td><a href="<?= site_url() ?>controlling/target?id=<?= $data['id'] ?>" class="btn btn-sm btn-block btn-primary" data-toggle="tooltip" data-placement="top" title="Monitoring Aplikasi ini!"><span class="ficon bx bxs-report"></span></a></td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
		<!-- END: Content-->
		<?= $footer ?>
		<!-- END: Footer-->

		<div class="modal fade text-left w-100" id="userForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="titleForm"></h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<i class="bx bx-x"></i>
						</button>
					</div>
					<div class="modal-body">
						<form class="form form-vertical" id="myForm">
							<input type="hidden" name="id" id="id">
							<div class="form-body">
								<div class="row">
									<div class="col-md-6 col-12">
										<div class="form-group">
											<label>Nama Target</label>
											<input type="text" class="form-control" name="nama" id="nama" placeholder="ex: Portal Berita Pemkab. Sampang">
										</div>
									</div>
									<div class="col-md-6 col-12">
										<div class="form-group">
											<label>Link Target</label>
											<input type="text" class="form-control" name="link" id="link" placeholder="ex: https://sampangkab.go.id/beranda/">
										</div>
									</div>
									<div class="col-md-6 col-12">
										<div class="form-group">
											<label>Status Notifikasi</label>
											<select name="status" id="status" class="form-control" onchange="dinamisSelect(this.value)">
												<option disabled selected>-- Pilih status notifikasi --</option>
												<option value="group">Group</option>
												<option value="personal">Personal</option>
											</select>
										</div>
									</div>
									<div class="col-md-6 col-12">
										<div class="form-group">
											<label>Status Notifikasi</label>
											<select name="gou" id="gou" class="form-control"></select>
										</div>
									</div>
									<div class="col-md-12 col-12">
										<div class="form-group">
											<label>Description</label>
											<input type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="ex: ini adalah portal berita sampang menggunakan CMS Wordpress dengan plugin ....">
										</div>
									</div>
									<div class="col-md-12 col-12">
										<div class="form-group">
											<label>Pattern Directory</label>
											<input type="text" class="form-control" name="folder" id="folder" placeholder="ex: /var/www/html/portal_berita">
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-light-secondary" id="btnClose" onclick="closeForm()">
							<i class="bx bx-x d-block d-sm-none"></i>
							<span class="d-none d-sm-block">Close</span>
						</button>
						<button type="button" class="btn btn-warning ml-1" id="btnEdit" onclick="actEdit()">
							<i class="bx bx-edit d-block d-sm-none"></i>
							<span class="d-none d-sm-block">Edit</span>
						</button>
						<button type="button" class="btn btn-danger ml-1" id="btnHapus" onclick="actHapus()">
							<i class="bx bx-trash d-block d-sm-none"></i>
							<span class="d-none d-sm-block">Hapus</span>
						</button>
						<button type="button" class="btn btn-primary ml-1" id="btnPush" onclick="pushData()">
							<i class="bx bx-check d-block d-sm-none"></i>
							<span class="d-none d-sm-block">Simpan</span>
						</button>
					</div>
				</div>
			</div>
		</div>

		<!-- BEGIN: Vendor JS-->
		<?= $javascript ?>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
		<!-- END: Page JS-->

		<script>

			function dinamisSelect(status) {
				$.ajax({
					url: "<?= site_url() ?>manage_target/on_change/"+status,
					method: "GET",
					success: function (data) {
						$("#gou").html(data);
					}
				})
			}

			function tambahUser() {
				$("#titleForm").html("From Tambah Target");
				$("#btnEdit").hide();
				$("#btnHapus").hide();
				$("#btnPush").show();
				activeEdit();
				$("#userForm").modal("show");
			}
			function closeForm() {
				$("#myForm")[0].reset();
				$("#btnEdit").show();
				$("#btnHapus").show();
				$("#btnPush").hide();
				$("#userForm").modal("hide");
			}
			function pushData() {
				$.ajax({
					url: "<?= site_url() ?>manage_target/do_manage_target",
					type: "POST",
					data: $("#myForm").serialize(),
					success: function (data) {
						if(data==="success"){
							location.reload();
						}else{
							closeForm();
							alert("Terjadi masalah, segera hubungi admin");
						}
					}
				})
			}

			function detailUser(id) {
				$.ajax({
					url: "<?= site_url() ?>manage_target/do_where/"+id,
					type: "GET",
					dataType: "JSON",
					success: function (data) {
						$("#titleForm").html("Detail Target Monitoring");
						$("#id").val(data.id);
						$("#nama").val(data.label_target);
						$("#link").val(data.link_apps);
						$("#deskripsi").val(data.description);
						$("#folder").val(data.directory);
						$("#status").val(data.status);
						$("#status").change();
						$("#gou").val(data.id_telegram);
						$("#btnPush").hide();
						noActiveEdit();
						$("#userForm").modal("show");
					}
				})
			}

			function noActiveEdit() {
				$("#nama").attr("readonly", true);
				$("#link").attr("readonly", true);
				$("#folder").attr("readonly", true);
				$("#deskripsi").attr("readonly", true);
				$("#status").attr("disabled", true);
				$("#gou").attr("disabled", true);
			}

			function activeEdit() {
				$("#nama").attr("readonly", false);
				$("#link").attr("readonly", false);
				$("#folder").attr("readonly", false);
				$("#deskripsi").attr("readonly", false);
				$("#status").attr("disabled", false);
				$("#gou").attr("disabled", false);
			}

			function actEdit() {
				activeEdit();
				$("#btnPush").show();
				$("#btnEdit").hide();
				$("#btnHapus").hide();
			}

			function actHapus() {
				Swal.fire({
					title: 'Apakah anda yakin?',
					text: "Pastikan benar bahwa anda ingin hapus data ini!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, hapus!'
				}).then((result) => {
					if (result.value) {
						$.ajax({
							url: "<?= site_url() ?>manage_target/do_delete/"+$("#id").val(),
							type: "GET",
							success: function (data) {
								if(data==="success"){
									Swal.fire(
										'Deleted!',
										'Data berhasil dihapus.',
										'success'
									);
									location.reload();
								}else{
									closeForm();
									alert("Terjadi masalah, segera hubungi admin");
								}
							}
						});
					}
				});
			}
		</script>
	</body>
</html>
