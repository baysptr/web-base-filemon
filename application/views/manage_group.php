<html class="loading" lang="en" data-textdirection="ltr">
	<head>
		<?= $head ?>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css" rel="stylesheet">
	</head>
	<body class="horizontal-layout horizontal-menu navbar-sticky 2-columns footer-static" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
		<?= $header ?>

		<!-- BEGIN: Content-->
		<div class="app-content content">
			<div class="content-overlay"></div>
			<div class="content-wrapper">
				<div class="content-body"><!-- account setting page start -->
					<section id="page-account-settings">
						<div class="row">
							<div class="col-12">
								<div class="row">
									<!-- left menu section -->
									<div class="col-md-3 mb-2 mb-md-0 pills-stacked">
										<ul class="nav nav-pills flex-column">
											<li class="nav-item">
												<a class="nav-link d-flex align-items-center active" id="account-pill-general" data-toggle="pill"
												   href="#account-vertical-general" aria-expanded="true">
													<i class="bx bx-list-check"></i>
													<span>List Group</span>
												</a>
											</li>
											<li class="nav-item">
												<a class="nav-link d-flex align-items-center" id="account-pill-password" data-toggle="pill"
												   href="#account-vertical-password" aria-expanded="false">
													<i class="bx bxs-add-to-queue"></i>
													<span>Buat Group</span>
												</a>
											</li>
											<li class="nav-item">
												<a class="nav-link d-flex align-items-center" id="account-pill-info" data-toggle="pill"
												   href="#account-vertical-info" aria-expanded="false">
													<i class="bx bx-info-circle"></i>
													<span>Info</span>
												</a>
											</li>
										</ul>
									</div>
									<!-- right content section -->
									<div class="col-md-9">
										<div class="card">
											<div class="card-content">
												<div class="card-body">
													<div class="tab-content">
														<div role="tabpanel" class="tab-pane active" id="account-vertical-general" aria-labelledby="account-pill-general" aria-expanded="true">
															<div class="row">
																<?php foreach ($groups as $group){ ?>
																	<div class="col-md-6 col-sm-12">
																		<div class="card bg-primary text-white">
																			<div class="card-header">
																				<h4 class="card-title text-white"><strong data-toggle="tooltip" data-placement="top" title="Nama Group"><?= $group['label_group'] ?></strong></h4>
																				<a class="heading-elements-toggle">
																					<i class="bx bx-dots-vertical font-medium-3"></i>
																				</a>
																				<div class="heading-elements">
																					<ul class="list-inline mb-0">
																						<li>
																							<a data-action="collapse">
																								<i class="bx bx-chevron-down"></i>
																							</a>
																						</li>
																					</ul>
																				</div>
																			</div>
																			<div class="card-content collapse show">
																				<div class="card-body">
																					<p>
																						ID Group Telegram : <strong><?= $group['id_group_telegram'] ?></strong><br/>
																						Pembuat Group : <strong><?= $group['id_create_user']['nama'] ?></strong><br/>
																						Anggota :
																					</p>
																					<?php foreach ($group['arr_id_user'] as $it){ ?>
																						<div class="chip mr-1" data-toggle="tooltip" data-placement="top" title="Anggota Group">
																							<div class="chip-body">
																								<div class="avatar bg-info">
																									<i class="bx bx-user"></i>
																								</div>
																								<span class="chip-text"><?= $it['nama'] ?></span>
																							</div>
																						</div>
																					<?php } ?>
																				</div>
																			</div>
																			<div class="card-footer" style="text-align: right">
																				<div class="row">
																					<div class="col-md-4 col-sm-12 btn btn-sm btn-block btn-warning" onclick="edtGroup('<?= $group['id'] ?>')" data-toggle="tooltip" data-placement="top" title="Ubah group ini?">Edit</div>
																					<div class="col-md-4"><div data-toggle="tooltip" data-placement="top" title="Test Bot" class="btn btn-sm btn-block btn-secondary" onclick="window.location='<?= site_url() ?>manage_group/test_bot/<?= ltrim($group["id_group_telegram"]) ?>'">Test Bot</div></div>
																					<div class="col-md-4 col-sm-12 btn btn-sm btn-block btn-danger" onclick="actHapus('<?= $group['id'] ?>')" data-toggle="tooltip" data-placement="top" title="Hapus group ini?">Delete</div>
																				</div>
																			</div>
																		</div>
																	</div>
																<?php } ?>
															</div>
														</div>
														<div class="tab-pane fade " id="account-vertical-password" role="tabpanel"
															 aria-labelledby="account-pill-password" aria-expanded="false">
															<form action="<?= site_url() ?>manage_group/do_insert" method="post">
																<div class="row">
																	<div class="col-md-6 col-sm-12">
																		<div class="form-group">
																			<label>Nama Group</label>
																			<input type="text" name="nama" id="nama" class="form-control" placeholder="ex: SysAdmin Group">
																		</div>
																	</div>
																	<div class="col-md-6 col-sm-12">
																		<div class="form-group">
																			<label>ID Group - Telegram, <small>Cek info, untuk cari tahu</small></label>
																			<input type="text" name="id_group" id="id_group" class="form-control" placeholder="ex: -11233567">
																		</div>
																	</div>
																	<div class="col-md-12 col-sm-12">
																		<div class="form-group">
																			<label>Anggota</label>
																			<div class="form-group">
																				<select class="select2 form-control" multiple="multiple" name="anggota[]" id="anggota">
																					<?php foreach ($users as $user){ ?>
																						<option value="<?= $user['id'] ?>"><?= $user['nama'] ?> [<?= $user['jabatan'] ?>]</option>
																					<?php } ?>
																				</select>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-12">
																		<button type="submit" class="btn btn-primary">Buat! <span class="ficon bx bxs-group"></span></button>
																	</div>
																</div>
															</form>
														</div>
														<div class="tab-pane fade" id="account-vertical-info" role="tabpanel" aria-labelledby="account-pill-info" aria-expanded="false">
															<ul class="widget-timeline mb-0">
																<li class="timeline-items timeline-icon-primary active">
																	<div class="timeline-time"><strong>Pertama</strong></div>
																	<h6 class="timeline-title">Buat Group</h6>
																	<div class="timeline-content">
																		<p>Buat group telegram anda dengan beberapa orang yang sudah memiliki account di <strong>file monitoring sistem</strong> ini, lalu tambahkan <strong>@kwkw_bot dan @mantengin_bot</strong> pada group anda, seperti gambar dibawah berikut.<br/><img src="<?= base_url() ?>assets/images/group_info.jpg" width="40%" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"></p>
																	</div>
																</li>
																<li class="timeline-items timeline-icon-primary active">
																	<div class="timeline-time"><strong>Kedua</strong></div>
																	<h6 class="timeline-title">Menemukan ID Group - Telegram anda</h6>
																	<div class="timeline-content">
																		<p>Setelah tahap pertama diatas selesai, maka ketika <strong>!id</strong> pada kolom chat group yang anda buat, maka anda akan menemukan ID - Group anda, setelah itu anda bisa test group anda pada <strong>tab, list group.</strong> seperti kolom dibawah berikut.<br/><img src="<?= base_url() ?>assets/images/id_group.png" width="40%" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"></p>
																	</div>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- account setting page ends -->
				</div>
			</div>
		</div>
		<!-- END: Content-->
		<?= $footer ?>
		<!-- END: Footer-->

		<div class="modal fade text-left w-100" id="groupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Edit Group</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<i class="bx bx-x"></i>
						</button>
					</div>
					<div class="modal-body">
						<form class="form form-vertical" id="formGroup" method="post" action="<?= site_url() ?>manage_group/do_update">
							<input type="hidden" name="id" id="id">
							<div class="form-body">
								<div class="row">
									<div class="col-md-6 col-sm-12">
										<div class="form-group">
											<label>Nama Group</label>
											<input type="text" name="edt_nama" id="edt_nama" class="form-control" placeholder="ex: SysAdmin Group">
										</div>
									</div>
									<div class="col-md-6 col-sm-12">
										<div class="form-group">
											<label>ID Group - Telegram, <small>Cek info, untuk cari tahu</small></label>
											<input type="text" name="edt_id_group" id="edt_id_group" class="form-control" placeholder="ex: -11233567">
										</div>
									</div>
									<div class="col-md-12 col-sm-12">
										<div class="form-group">
											<label>Anggota</label>
											<div class="form-group">
												<select class="select2 form-control" multiple="multiple" name="edt_anggota[]" id="edt_anggota">
													<?php foreach ($users as $user){ ?>
														<option value="<?= $user['id'] ?>"><?= $user['nama'] ?> [<?= $user['jabatan'] ?>]</option>
													<?php } ?>
												</select>
											</div>
										</div>
									</div>
									<div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
										<button type="submit" class="btn btn-primary glow mr-sm-1 mb-1">Ubah</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-light-secondary" id="btnClose" onclick="closeModal()">
							<i class="bx bx-x d-block d-sm-none"></i>
							<span class="d-none d-sm-block">Close</span>
						</button>
					</div>
				</div>
			</div>
		</div>

		<!-- BEGIN: Vendor JS-->
		<?= $javascript ?>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
		<!-- END: Page JS-->
		<script>
			$(".select2").select2({dropdownAutoWidth:!0,width:"100%"});
			function actHapus(id) {
				if(confirm("Apakah anda yakin akan hapus data ini?")===true){
					window.location='<?= site_url() ?>manage_group/do_delete/'+id;
				}
			}
			function edtGroup(id) {
				$.ajax({
					url: "<?= site_url() ?>manage_group/do_where/"+id,
					method: "GET",
					dataType: "JSON",
					success: function (data) {
						var sel = data.arr_id_user;
						sel = sel.split(",");
						console.log(sel);
						$("#id").val(data.id);
						$("#edt_nama").val(data.label_group);
						$("#edt_id_group").val(data.id_group_telegram);
						$("#edt_anggota").val(sel);
						$("#edt_anggota").select2({dropdownAutoWidth:!0,width:"100%"}).trigger('change');
						$("#groupModal").modal("show");
					}
				})
			}
			function closeModal() {
				$("#groupModal").modal("hide");
			}
		</script>
	</body>
</html>
