<html class="loading" lang="en" data-textdirection="ltr">
	<head>
		<?= $head ?>
	</head>
	<body class="horizontal-layout horizontal-menu navbar-sticky 2-columns footer-static" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
		<?= $header ?>

		<!-- BEGIN: Content-->
		<div class="app-content content">
			<div class="content-overlay"></div>
			<div class="content-wrapper">
				<div class="content-header row">
				</div>
				<div class="content-body">
					<!-- Dashboard Analytics Start -->
					<section id="dashboard-analytics">
						<div class="row">
							<!-- Website Analytics Starts-->
							<div class="col-md-4 col-sm-12">
								<div class="card">
									<div class="card-header d-flex justify-content-between align-items-center">
										<h4 class="card-title">Average of Files</h4>
									</div>
									<div class="card-content">
										<div class="card-body pb-1 text-center">
											<div id="bar_code"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-8 col-sm-12">
								<div class="card">
									<div class="card-header d-flex justify-content-between align-items-center">
										<h4 class="card-title">Threat of Distribution</h4>
									</div>
									<div class="card-content">
										<div class="card-body pb-1 text-center">
											<div id="column_bar"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-8 col-sm-12">
								<div class="card">
									<div class="card-header d-flex justify-content-between align-items-center">
										<h4 class="card-title">Changes Files of Distribution</h4>
									</div>
									<div class="card-content">
										<div class="card-body pb-1">
											<table class="table table-responsive">
												<thead>
													<tr>
														<td><strong>NAMA</strong></td>
														<td><strong>BEHAVIOUR</strong></td>
														<td><strong>IGNORES</strong></td>
														<td>&nbsp;</td>
													</tr>
												</thead>
												<tbody>
													<?php foreach ($changes as $change){ ?>
														<tr>
															<td><a href="<?= $change['link_apps'] ?>" target="_blank"><?= $change['label_target'] ?></a> </td>
															<td><?= $change['many_change'] ?> File</td>
															<td><?= $change['cf'] ?> File</td>
															<td><a href="<?= site_url() ?>controlling/target?id=<?= $change['id'] ?>" style="color: white" class="btn btn-block btn-sm btn-warning"><span class="ficon bx bx-run"></span></a></td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12">
								<div class="card">
									<div class="card-header d-flex justify-content-between align-items-center">
										<h4 class="card-title">Ignore of Files</h4>
									</div>
									<div class="card-content">
										<div class="card-body pb-1 text-center">
											<div id="ignore_bar"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12">
								<div class="card">
									<div class="card-header d-flex justify-content-between align-items-center">
										<h4 class="card-title">Latest of Change File</h4>
									</div>
									<div class="card-content">
										<div class="card-body pb-1 text-center">
											<table class="table table-responsive">
												<thead>
													<tr>
														<td><strong>NAMA APPS</strong></td>
														<td><strong>FILENAME</strong></td>
														<td><strong>MODE</strong></td>
														<td><strong>MALICIOUS</strong></td>
													</tr>
												</thead>
												<tbody>
													<?php foreach ($latests as $latest){ ?>
														<tr>
															<td><?= $latest['label_target'] ?></td>
															<td><?= $latest['filename'] ?></td>
															<td><?php if($latest['mode_file'] == "C"){ echo "CREATED"; }elseif ($latest['mode_file']=="M"){echo "MODIFIED";}else{echo"DELETED";} ?></td>
															<td><?= $latest['scan_detector'] ?></td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
		<!-- END: Content-->
		<?= $footer ?>
		<!-- END: Footer-->

		<!-- BEGIN: Vendor JS-->
		<?= $javascript ?>
		<script type="text/javascript" src="<?= base_url() ?>assets/vendors/js/charts/apexcharts.min.js"></script>
		<script type="text/javascript">
			var chart = new ApexCharts(document.querySelector("#bar_code"), {
				series: [<?= $count_detector['clean_code'] ?>, <?= $count_detector['all_threat'] ?>],
				chart: {
					type: 'pie'
				},
				labels: ['Clean Code', 'Threat Code'],
				dataLabels: {
					enabled: true,
					formatter: function (val) {
						return Math.floor(val) + " Files"
					}
				},
				legend: {
					position: 'top'
				},
				responsive: [{
					breakpoint: 480,
					options: {
						chart: {
							width: 200
						},
						legend: {
							position: 'bottom'
						}
					}
				}]
			});
			chart.render();

			var bar = new ApexCharts(document.querySelector("#column_bar"), {
				series: [{
					data: [
						<?= $count_detector['PasswordProtection'] ?>,
						<?= $count_detector['ObfuscatedPhp'] ?>,
						<?= $count_detector['DangerousPhp'] ?>,
						<?= $count_detector['HiddenInAFile'] ?>,
						<?= $count_detector['CloudFlareBypass'] ?>,
						<?= $count_detector['SuspiciousEncoding'] ?>,
						<?= $count_detector['Websites'] ?>,
						<?= $count_detector['DodgyPhp'] ?>,
						<?= $count_detector['DodgyStrings'] ?>
					]
				}],
				chart: {
					height: 310,
					type: 'bar'
				},
				plotOptions: {
					bar: {
						columnWidth: '45%',
						distributed: true
					}
				},
				dataLabels: {
					enabled: false
				},
				legend: {
					show: false
				},
				xaxis: {
					categories: [
						'PasswordProtection',
						'ObfuscatedPhp',
						'DangerousPhp',
						'HiddenInAFile',
						'CloudFlareBypass',
						'SuspiciousEncoding',
						'Websites',
						'DodgyPhp',
						'DodgyStrings'
					],
					labels: {
						style: {
							fontSize: '12px'
						}
					}
				}
			});
			bar.render();

			var ignore_bar = new ApexCharts(document.querySelector("#ignore_bar"), {
				series: [<?= $ignore->cf ?>, <?= $ignore->all_file ?>],
				chart: {
					type: 'pie'
				},
				labels: ['Ignore File', 'All File'],
				dataLabels: {
					enabled: true,
					formatter: function (val) {
						return Math.floor(val) + " %"
					}
				},
				legend: {
					position: 'top'
				},
				responsive: [{
					breakpoint: 480,
					options: {
						chart: {
							width: 200
						},
						legend: {
							position: 'bottom'
						}
					}
				}]
			});
			ignore_bar.render();
		</script>
	</body>
</html>
